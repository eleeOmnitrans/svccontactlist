﻿Imports System.Web.SessionState

Public Class Global_asax
    Inherits System.Web.HttpApplication

    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        Server.Transfer("/Presentation/OmniErrorPage.aspx") ' if error is raised then redirect to OmniErrorPage web page
    End Sub


    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        Session("AssignmentMgr") = Nothing
    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        Session("AssignmentMgr") = Nothing
    End Sub
End Class