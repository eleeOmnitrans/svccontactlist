﻿Imports CrystalDecisions.CrystalReports.Engine

Public Class ReportMgr

#Region "Class Properties"

    Private mReport As ReportDocument
    Public Property Report() As ReportDocument
        Get
            Return mReport
        End Get
        Set(ByVal value As ReportDocument)
            mReport = value
        End Set
    End Property

#End Region

#Region "Constructor"

    Public Sub New()
        Me.Report = New ReportDocument
    End Sub

#End Region

#Region "Class Mthods"

    ''' <summary>
    ''' Get report loaded by datatable
    ''' </summary>
    ''' <param name="DT"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetReportLoadedByDT(ByVal DT As Object) As CrystalDecisions.CrystalReports.Engine.ReportDocument
        Me.Report = GetReport()
        Me.Report.SetDataSource(DT)
        Return Me.Report
    End Function

    ''' <summary>
    ''' Return New Service Contact List Report
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetReport() As CrystalDecisions.CrystalReports.Engine.ReportDocument
        Return New SvcContactListRpt
    End Function

    ''' <summary>
    ''' Close and dispose/release report instance in memory
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub Dispose()
        Me.Report.Close()
        Me.Report.Dispose()
    End Sub

#End Region
End Class
