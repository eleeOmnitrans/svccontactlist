﻿Public Class StaffAssignmentMgr

#Region "Class Properties"

    Private mStaffAssignmentsGW As StaffAssignmentsGateway

    Public Property StaffAssignmentsGW() As StaffAssignmentsGateway
        Get
            Return mStaffAssignmentsGW
        End Get
        Set(ByVal value As StaffAssignmentsGateway)
            mStaffAssignmentsGW = value
        End Set
    End Property


    Private mLang As String
    Public Property Lang() As String
        Get
            Return mLang
        End Get
        Set(ByVal value As String)
            mLang = value
        End Set
    End Property

#End Region

#Region "Class Methods"

    ''' <summary>
    ''' New gateway
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub New()
        Me.StaffAssignmentsGW = New StaffAssignmentsGateway
    End Sub

    ''' <summary>
    ''' Create, insert and load StaffAssignments for a new client
    ''' </summary>
    ''' <param name="ClientID"></param>
    ''' <remarks></remarks>
    Public Sub CreateInsertLoadStaffAssignmentsForNewClient(ByVal ClientID As String)
        Me.StaffAssignmentsGW.LoadNewClient(ClientID)
        Me.StaffAssignmentsGW.Insert()
        LoadStaffAssignmentsForExistingClient(ClientID)
    End Sub

    ''' <summary>
    ''' Update StaffAssignments Table
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub Update()
        Me.StaffAssignmentsGW.Update()
    End Sub

    ''' <summary>
    ''' Load StaffAssignments for an existing client
    ''' </summary>
    ''' <param name="ClientID"></param>
    ''' <remarks></remarks>
    Sub LoadStaffAssignmentsForExistingClient(ByVal ClientID As String)
        Me.StaffAssignmentsGW.LoadExistingClient(ClientID, Me.Lang)
    End Sub

    ''' <summary>
    ''' Load by Client ID
    ''' </summary>
    ''' <param name="ClientID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function LoadByClientID(ByVal ClientID As String) As Boolean
        Me.StaffAssignmentsGW.LoadExistingClient(ClientID, Me.Lang)
        If Me.StaffAssignmentsGW.RowCount >= 1 Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function Table() As DataTable
        Return Me.StaffAssignmentsGW.Table
    End Function


    Public Sub InsertDefault(ByVal ClientName As String, ByVal DeptID As Integer, ByVal DeptPositionID As Integer)
        Const DEPTDEFAULT As Integer = 1
        Dim staffGW As New StaffGateway
        staffGW.LoadWhere(String.Format("DeptID = {0} and DeptDefault = {1}", DeptID, DEPTDEFAULT))
        Dim defaultStaffID As Integer = staffGW.Table.Rows(0).Item("ID")
        Me.StaffAssignmentsGW.AddNew()
        Dim staffAssignmentDR As DataRow = Me.StaffAssignmentsGW.Table.NewRow
        With staffAssignmentDR
            .Item("ClientID") = ClientName
            .Item("DeptPositionID") = DeptPositionID
            .Item("DeptID") = DeptID
            .Item("StaffID") = defaultStaffID
            .Item("LastChanged") = Now()
        End With
        Me.StaffAssignmentsGW.Table.Rows.Add(staffAssignmentDR)
        Me.StaffAssignmentsGW.Insert()
    End Sub

    ''' <summary>
    ''' Check StaffAssignments for an existing position
    ''' </summary>
    ''' <param name="ClientID"></param>
    ''' <remarks></remarks>
    Function CheckStaffAssignmentsForExistingPosition(ByVal ClientID As String, ByVal DeptPositionID As Integer) As Boolean
        Me.StaffAssignmentsGW.LoadWhere(String.Format("ClientID = '{0}' and  DeptPositionID = {1}", ClientID, DeptPositionID.ToString))
        Return Me.StaffAssignmentsGW.RowCount > 0
    End Function
#End Region

End Class
