﻿Imports System.Data.SqlClient

Public Class StaffAssignmentsGateway
    Inherits DataGatewayBase

    Sub New()
        MyBase.TableName = "StaffAssignments"
        MyBase.mConnectStr = GetAppDBConnectStr()
    End Sub

    ''' <summary>
    ''' set to typed data table or non-typed data table
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Overrides Function newDataTable() As DataTable
        Return New StaffAssignments.StaffAssignmentsDataTable
    End Function

#Region "Class Methods"

    ''' <summary>
    ''' Insert records (from datable) to database.
    ''' </summary>
    Public Function Insert() As Integer

        Dim retval As Integer = 0
        Const SPROCNAME As String = "StaffAssignments_Insert"
 

        For Each dr As DataRow In Me.Table.Rows

            Dim sqlCmd As New SqlCommand(SPROCNAME)
            Dim retStaffAssignmentsID As New SqlParameter("@ID", SqlDbType.Int)


            sqlCmd.Parameters.Add(retStaffAssignmentsID)
            retStaffAssignmentsID.Direction = ParameterDirection.Output
            sqlCmd.Parameters.AddWithValue("@ClientID", dr.Item("ClientID"))
            sqlCmd.Parameters.AddWithValue("@DeptPositionID", dr.Item("DeptPositionID"))
            sqlCmd.Parameters.AddWithValue("@StaffID", dr.Item("StaffID"))
            sqlCmd.Parameters.AddWithValue("@DeptID", dr.Item("DeptID"))
            ' sqlCmd.Parameters.AddWithValue("@SubDeptName", dr.Item("SubDeptName"))
            sqlCmd.Parameters.AddWithValue("@LastChanged", dr.Item("LastChanged"))
            sqlCmd.Parameters.AddWithValue("@NotDisplayed", dr.Item("NotDisplayed"))

            ExecSQLSProcHelper(sqlCmd)
            retval = CInt(retStaffAssignmentsID.Value)
            sqlCmd.Dispose()

        Next
        Return retval
    End Function

    Public Sub Update()
        Const SPROCNAME As String = "StaffAssignments_Update"

        For Each dr As DataRow In Me.Table.Rows
            Dim sqlCmd As New SqlCommand(SPROCNAME)
            With dr
                sqlCmd.Parameters.AddWithValue("@ClientID", .Item("ClientID"))
                sqlCmd.Parameters.AddWithValue("@DeptPositionID", .Item("DeptPositionID"))
                sqlCmd.Parameters.AddWithValue("@StaffID", .Item("StaffID"))
                sqlCmd.Parameters.AddWithValue("@DeptID", .Item("DeptID"))
                sqlCmd.Parameters.AddWithValue("@LastChanged", CDateToDBDate(.Item("LastChanged")))
                sqlCmd.Parameters.AddWithValue("@NotDisplayed", .Item("NotDisplayed"))
            End With
            ExecSQLSProcHelper(sqlCmd)
        Next
    End Sub

    ''' <summary>
    ''' Load an existing client from the database
    ''' </summary>
    ''' <param name="ClientID"></param>
    ''' <remarks></remarks>
    Public Sub LoadExistingClient(ByVal ClientID As String, ByVal Lang As String)
        Const SPROCNAME As String = "StaffAssignments_SelectExistingClient"
        Dim sqlCmd As New SqlCommand(SPROCNAME)
        sqlCmd.Parameters.AddWithValue("@ImanetID", ClientID)
        sqlCmd.Parameters.AddWithValue("@Lang", Lang)
        LoadWhere(sqlCmd)
    End Sub

    Public Sub LoadAllClient()
        Const SPROCNAME As String = "StaffAssignments_SelectAllClient"
        Dim sqlCmd As New SqlCommand(SPROCNAME)
        LoadWhere(sqlCmd)
    End Sub

    ''' <summary>
    ''' Load a new client from the database
    ''' </summary>
    ''' <param name="ClientID"></param>
    ''' <remarks></remarks>
    Public Sub LoadNewClient(ByVal ClientID As String)
        Const SPROCNAME As String = "StaffAssignments_SelectNewClient"
        Dim sqlCmd As New SqlCommand(SPROCNAME)
        sqlCmd.Parameters.AddWithValue("@ImanetID", ClientID)
        LoadWhere(sqlCmd)
    End Sub

#End Region

#Region "DataGateway code"
#End Region

#Region "Dataset Holder Code"

#End Region

#Region "TestCode"
#End Region

#Region "Code No Longer Used"
#End Region

End Class
