﻿Imports System.Data.SqlClient

''' <summary>
''' Manages all database interactions.  Uses Typed datasets as data transfer objects to exchange data between biz and data layer.
''' </summary>

Public Class StaffGateway
    Inherits DataGatewayBase

    Sub New()
        MyBase.TableName = "Staff"
        MyBase.mConnectStr = GetAppDBConnectStr()
    End Sub

    ''' <summary>
    ''' set to typed data table or non-typed data table
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Overrides Function newDataTable() As DataTable
        Return New Staff.StaffDataTable
    End Function

#Region "Class Methods"

    'Sub LoadStaffByID(ByVal WhereClause As String)
    '    Me.LoadWhere(WhereClause)
    'End Sub

    ''' <summary>
    ''' Insert records (from datable) to database.
    ''' </summary>
    Public Function Insert(ByVal DR As DataRow) As Integer
        Dim retval As Integer = 0
        Dim retStaffID As New SqlParameter("@ID", SqlDbType.Int)
        Const SPROCNAME As String = "Staff_Insert"

        With DR
            Dim sqlCmd As New SqlCommand(SPROCNAME)
            sqlCmd.Parameters.Add(retStaffID)
            retStaffID.Direction = ParameterDirection.Output
            sqlCmd.Parameters.AddWithValue("@LastName", .Item("LastName"))
            sqlCmd.Parameters.AddWithValue("@FirstName", .Item("FirstName"))
            sqlCmd.Parameters.AddWithValue("@Tel", .Item("Tel"))
            sqlCmd.Parameters.AddWithValue("@TelExt", .Item("TelExt"))
            sqlCmd.Parameters.AddWithValue("@Fax", .Item("Fax"))
            sqlCmd.Parameters.AddWithValue("@Email", .Item("Email"))
            sqlCmd.Parameters.AddWithValue("@Cell", .Item("Cell"))
            sqlCmd.Parameters.AddWithValue("@Notes", .Item("Notes"))
            sqlCmd.Parameters.AddWithValue("@DeptID", .Item("DeptID"))
            sqlCmd.Parameters.AddWithValue("@LastChanged", .Item("LastChanged"))
            sqlCmd.Parameters.AddWithValue("@DeptDefault", .Item("DeptDefault"))
            sqlCmd.Parameters.AddWithValue("@LastNameFR", .Item("LastNameFR"))
            sqlCmd.Parameters.AddWithValue("@FirstNameFR", .Item("FirstNameFR"))
            sqlCmd.Parameters.AddWithValue("@Active", .Item("Active"))
            ExecSQLSProcHelper(sqlCmd)
            retval = CInt(retStaffID.Value)
            sqlCmd.Dispose()
        End With
        Return retval
    End Function

    Public Sub Update(ByVal DTRow As DataRow)
        Const SPROCNAME As String = "Staff_Update"
        Dim sqlCmd As New SqlCommand(SPROCNAME)
        ' With Me.Table.Rows(0)
        sqlCmd.Parameters.AddWithValue("@ID", DTRow.Item("ID"))
        sqlCmd.Parameters.AddWithValue("@LastName", DTRow.Item("LastName"))
        sqlCmd.Parameters.AddWithValue("@FirstName", DTRow.Item("FirstName"))
        sqlCmd.Parameters.AddWithValue("@Tel", DTRow.Item("Tel"))
        sqlCmd.Parameters.AddWithValue("@TelExt", DTRow.Item("TelExt"))
        sqlCmd.Parameters.AddWithValue("@Fax", DTRow.Item("Fax"))
        sqlCmd.Parameters.AddWithValue("@Email", DTRow.Item("Email"))
        sqlCmd.Parameters.AddWithValue("@Cell", DTRow.Item("Cell"))
        sqlCmd.Parameters.AddWithValue("@Notes", DTRow.Item("Notes"))
        sqlCmd.Parameters.AddWithValue("@DeptID", DTRow.Item("DeptID"))
        sqlCmd.Parameters.AddWithValue("@LastChanged", DTRow.Item("LastChanged"))
        sqlCmd.Parameters.AddWithValue("@Active", DTRow.Item("Active"))
        ' End With
        ExecSQLSProcHelper(sqlCmd)
    End Sub

    Public Sub UpdateByID(ByVal SqlTxt As String)
        Dim sqlCmd As New SqlCommand(SqlTxt)
        ExecSQLCmdTxtHelper(sqlCmd)
    End Sub

#End Region

#Region "DataGateway code"
#End Region

#Region "Dataset Holder Code"

#End Region

#Region "TestCode"
#End Region

#Region "Code No Longer Used"
#End Region


End Class
