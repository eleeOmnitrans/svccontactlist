﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="OmniErrorPage.aspx.vb"    Inherits="SvcContactListWeb.UniversalErrorPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title>Service Contact List</title>
    <style type="text/css">
       .style3
       {
          font-weight: normal;
       }
       .style4
       {
          font-weight: bold;
          white-space: nowrap;
       }
         .style5
       {
          font-weight: normal;
          white-space:pre;
       }
    </style>
</head>
<body>
      <form id="form1" runat="server">
      <img alt="Omnitrans" src="Omnitrans.jpg" />
      <br />
      <br />
      <table width="100%" cellspacing="5">
         <tr>
            <td class="style4">
               &nbsp;Error Code: 
            </td>
            <td class="style3">
               &nbsp;<asp:Label class="style3" ID="lblErrorTxt" runat="server" Text=""></asp:Label>
            </td>
         </tr>
         <tr>
            <td class="style4">
               &nbsp;Description: 
            </td>
            <td class="style3">
               &nbsp;<asp:Label class="style3" ID="lblErrorTxt2" runat="server" Text=""></asp:Label>
            </td>
         </tr>
         <tr>
            <td class="style4">
               &nbsp;Source:
            </td>
            <td class="style3">
               &nbsp;<asp:Label class="style3" ID="lblErrorTxt3" runat="server" Text=""></asp:Label>
            </td>
         </tr>
         <tr>
            <td class="style4" valign="top">
               &nbsp;Stack Trace:
            </td>
            <td class="style3" valign="top">&nbsp;<asp:Label class="style5" ID="lblErrorTxt4" runat="server" Text=""></asp:Label>
            </td> 
         </tr>
      </table>
      </form>
    
</body>
</html>
