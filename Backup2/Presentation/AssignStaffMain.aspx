<%@ Page Title="Contact Sheet" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site1.Master" CodeBehind="AssignStaffMain.aspx.vb" Inherits="SvcContactListWeb.AssignStaffMain" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" EnableViewStateMac="False" runat="server">
   
     
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <table cellspacing="0" cellpadding="0" border="0" 
        style="height: 21px; width: 1100px">
        <tr>
            <td align="right" style="font-family: Arial">
               <asp:Panel ID="panelUpdate" class="style6" runat="server" Visible="true" BorderColor="Transparent">
                Admin Use Only &nbsp; &nbsp;
                <asp:LinkButton ID="lbStaffUpdateLink" class="style8" PostBackUrl="StaffMain.aspx"
                    runat="server" Style="text-decoration: none">Update Staff</asp:LinkButton>
                &nbsp; &nbsp;
                <asp:LinkButton ID="lbDeptPosition" class="style8" PostBackUrl="DeptPositionMain.aspx"
                    runat="server" Style="text-decoration: none">Update Position</asp:LinkButton>
               </asp:Panel>
           </td>
        </tr>
   </table>
    
   <table align="left" border="0" cellpadding="0" cellspacing="0">
       <tr valign="middle" >
        <td nowrap="nowrap" align="right" valign="middle" style="font-family: Arial">Client:&nbsp; &nbsp; </td>
        <td nowrap="nowrap" align="left" valign="middle" style="font-family: Arial">
                 <asp:TextBox ID="tbClient" autocomplete ="off" runat="server" 
                         style="margin-left: 0px" Height="21px" Width="346px"></asp:TextBox> &nbsp;
                 <asp:Button ID="btnFind" runat="server" Text="Find" />
            <div style="display: none">
                <input type="text" name="hiddenText" />
            </div>
            <asp:Button ID="btnGo" runat="server" Text="Create New" />
            &nbsp; &nbsp;
        </td>
        <td nowrap="nowrap">
            <asp:Panel ID="panelPrint" class="style6" runat="server" Visible="false">
                <asp:RadioButtonList ID="rbLang" runat="server" Width="165px" AutoPostBack="False"
                RepeatColumns="2" RepeatDirection="Horizontal" RepeatLayout="Flow" > 
                <asp:ListItem Selected="True">English</asp:ListItem><asp:ListItem>French</asp:ListItem></asp:RadioButtonList>
                <asp:RadioButtonList ID="rbCompany" runat="server" RepeatDirection="Horizontal" Width="267px"
                    AutoPostBack="False" RepeatColumns="3" RepeatLayout="Flow" Height="23px">
                    <asp:ListItem Selected="True" Value="Omni">Omnitrans</asp:ListItem>
                    <asp:ListItem Value="JRH">JR Hebert</asp:ListItem>
                    <asp:ListItem>Metro</asp:ListItem>
                </asp:RadioButtonList> &nbsp; &nbsp; 
                <asp:Button ID="btnPreviewRpt" runat="server" Text="Print" OnClick="PreviewRpt" />
            </asp:Panel>
            <asp:Panel ID="panelInsertDept" class="style6" Wrap="false" runat="server" Visible="false" BorderColor="Transparent">
            &nbsp; &nbsp;<asp:LinkButton ID="lblInsertDept" class="style8"   OnCommand="InsertDeptPosition"
                    runat="server" Style="text-decoration: none">Insert Dept</asp:LinkButton>
            </asp:Panel>
            
       </td>
      </tr>
      <tr> 
         <td align="right" valign="middle"  style="font-family: Arial">&nbsp; &nbsp;</td>
         <td align="left" style="font-family: Arial">
             <asp:ListBox ID="ClientList" runat="server" OnSelectedIndexChanged="ClientList_SelectedIndexChanged" AutoPostBack="true"  Style="margin-left: 0px" height="200px"  >
             </asp:ListBox>
         </td>
          <td align="right" valign="middle" style="font-family: Arial">
              &nbsp; &nbsp;
          </td>
      </tr>
   </table><br />
    <br />
    <br />
    <br />
    <table align="left" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left" style="font-family: Arial">
             <asp:Label ID="lblClientName" runat="server" Text=""></asp:Label>
            </td>
        </tr>
      <tr>
        <td align="left" style="font-family: Arial">
       <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="8"
        OnRowDataBound="ColorRowsHiddenRows" AutoGenerateEditButton="True"   DataKeyNames="ID,Name"
        Style="font-family: Verdana; white-space: nowrap;" Font-Size="Small" Width="100%">
    <Columns>
            <asp:BoundField DataField="Name" HeaderText="Department" Visible="True" ReadOnly="True"  >
                <ItemStyle Wrap="False"  CssClass="style7" />
            </asp:BoundField>
                
           <asp:BoundField DataField="PositionDesc" HeaderText="Position Description" Visible="True" ReadOnly="True" >
               <ItemStyle Wrap="False" CssClass="style7" />
           </asp:BoundField>
      
        <asp:TemplateField Visible="true" HeaderText="Display on<br/>Print"  HeaderStyle-Wrap="false" HeaderStyle-HorizontalAlign="Center">
            <ItemStyle Wrap="False" CssClass="style7" />
            <ItemTemplate>
                <%#If(Eval("NotDisplayed").ToString().Equals("True"), "Hidden", "")%>
            </ItemTemplate>
        </asp:TemplateField>
        
        <asp:BoundField DataField="SubDeptName" HeaderText="Sub Department" Visible="True"
            ReadOnly="True">
            <ItemStyle Wrap="False" CssClass="style7" />
        </asp:BoundField>
        
            <asp:TemplateField HeaderText="Contact" >
                <ItemStyle Wrap="False" CssClass="style7" />
            <ItemTemplate>
                <%#Eval("StaffFirstName") & " " & Eval("StaffLastName")%>
            </ItemTemplate>
            <EditItemTemplate>
            <asp:DropDownList ID="dropDownStaff"  DataSource="<%# GetStaffFullName() %>" DataTextField="FullName" DataValueField="ID" runat="server" SelectedValue='<%# Eval("StaffID").ToString() %>'>
                </asp:DropDownList>
            </EditItemTemplate>

            </asp:TemplateField>
          
        <asp:TemplateField Visible="true" HeaderText="Tel">
            <ItemStyle Wrap="False" CssClass="style7" />
            <ItemTemplate>
                <%#Eval("Tel").ToString() & " " & Eval("TelExt").ToString()%>
            </ItemTemplate>
        </asp:TemplateField>
        
        <asp:BoundField DataField="Email" HeaderText="Email" Visible="True" ReadOnly="True">
            <ItemStyle Wrap="False" CssClass="style7" />
        </asp:BoundField>
            
            
            <asp:BoundField DataField="LastChanged" HeaderText="Date<br/>Assigned" Visible="True"
                HtmlEncode="false" ReadOnly="True" DataFormatString="{0:yyyy/MM/dd}">
                <ItemStyle Wrap="False" CssClass="style7" />
          </asp:BoundField>
    </Columns>
    
    </asp:GridView>
    
         </td>
      </tr>
    </table>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
       <br />
        
</asp:Content>
