﻿Partial Public Class UniversalErrorPage
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        InitPage()
    End Sub

    ''' <summary>
    ''' Get error text and populate labels on error web page
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub InitPage()
        Dim appException As System.Exception = Server.GetLastError()
        If (TypeOf (appException) Is HttpException) Then
            Dim checkException As HttpException = CType(appException, HttpException)
            lblErrorTxt.Text = checkException.GetHttpCode.ToString
            lblErrorTxt2.Text = checkException.GetBaseException().Message
            lblErrorTxt3.Text = checkException.GetBaseException().Source
            lblErrorTxt4.Text = checkException.GetBaseException().ToString
        End If
        Server.ClearError()
    End Sub

End Class