﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Site1.Master"  CodeBehind="DeptPositionMain.aspx.vb"
    Inherits="SvcContactListWeb.DeptPositionMain" %>


<asp:content id="Content2" contentplaceholderid="ContentPlaceHolder1" runat="server">
        <br />
          <div style="font-family: Arial">
        Department  
              <asp:DropDownList ID="Dept" CssClass="style8" runat="server" DataTextField="Name"
            DataValueField="Name" OnSelectedIndexChanged="ChangeDept" AutoPostBack="True" />
        <br />
        <br />
        <br />
    <table align="left" border="0" cellpadding="0" cellspacing="0" >
    <tr><td align="left" style="font-family: Arial">
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" AutoGenerateEditButton="True"
            DataKeyNames="ID" Font-Size="Small"  Style="font-family: Verdana; white-space: nowrap;"
            CellPadding="8" Width="100%">
            <Columns>
            
                <asp:TemplateField Visible="true" HeaderStyle-Wrap="false" HeaderText="Position Description">
                    <ItemStyle  Wrap="false" CssClass="style7" />
                    <ItemTemplate>
                        <%#Eval("PositionDesc")%>
                    </ItemTemplate>
                    <EditItemTemplate>
                     <asp:TextBox ID="PositionDesc" runat="server" Value='<%# Eval("PositionDesc").ToString()%>'>
                     </asp:TextBox>
                    </EditItemTemplate>
                </asp:TemplateField>
                              
                <asp:TemplateField Visible="true" HeaderStyle-Wrap="false" HeaderText="Sub Department">
                    <ItemStyle Wrap="False" CssClass="style7" />
                    <ItemTemplate>
                        <%#Eval("SubDeptName")%>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="SubDeptName" runat="server" Value='<%# Eval("SubDeptName").ToString()%>'>
                        </asp:TextBox>
                    </EditItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField Visible="true" HeaderStyle-Wrap="false"  HeaderText="Position Description French">
                    <ItemStyle Wrap="False" CssClass="style7"  />
                    <ItemTemplate>
                        <%#Eval("PositionDescFR")%>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="PositionDescFR" runat="server" Value='<%# Eval("PositionDescFR").ToString()%>'>
                        </asp:TextBox>
                    </EditItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField Visible="true" HeaderStyle-Wrap="false" HeaderText="Sub Department French">
                    <ItemStyle Wrap="False" CssClass="style7"  />
                    <ItemTemplate>
                        <%#Eval("SubDeptNameFR")%>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="SubDeptNameFR" runat="server" Value='<%# Eval("SubDeptNameFR").ToString()%>'>
                        </asp:TextBox>
                    </EditItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField Visible="true" HeaderText="Active">
                    <ItemStyle Wrap="False" CssClass="style7" />
                    <ItemTemplate>
                        <%#If(Eval("Active").ToString().Equals("True"), "Yes", "No")%>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:CheckBox ID="Active" runat="server" Checked='<%# Convert.ToBoolean(Eval("Active")) %>'
                            Enabled="true" />
                    </EditItemTemplate>
                </asp:TemplateField>
                         
                <asp:TemplateField Visible="true" HeaderStyle-Wrap="false" HeaderText="Print Order">
                    <ControlStyle Width="15" />
                    <ItemStyle Wrap="False" CssClass="style7" />
                    <ItemTemplate>
                        <%#Eval("DisplayOrder")%>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="DisplayOrder" runat="server" Value='<%# Eval("DisplayOrder").ToString()%>'>
                        </asp:TextBox>
                    </EditItemTemplate>
                </asp:TemplateField>
              
             
            </Columns>
        </asp:GridView>
    </td>
    </tr>
        <tr><td align="right">
            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                <tr>
                    <td align="left">
                        <asp:LinkButton ID="lbAssignStaffMainLink" CssClass="style8" PostBackUrl="AssignStaffMain.aspx"
                            runat="server" Style="text-decoration: none"> Return to Main </asp:LinkButton>
                    </td>
                    <td align="right">
                        <asp:Button ID="btnAddPosition" runat="server" Text="Add Position" />
                    </td>
                </tr>
            </table>
            
        </td></tr>
        
    </table>
    </div>
 
</asp:content>
