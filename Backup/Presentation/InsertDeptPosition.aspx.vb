﻿Public Partial Class InsertDeptPosition
    Inherits System.Web.UI.Page

#Region "Class properties"

    Private mAssignmentMgr As StaffAssignmentMgr
    Public Property AssignmentMgr() As StaffAssignmentMgr
        Get
            mAssignmentMgr = Session("AssignmentMgr")
            Return mAssignmentMgr
        End Get
        Set(ByVal value As StaffAssignmentMgr)
            mAssignmentMgr = value
            Session("AssignmentMgr") = mAssignmentMgr
        End Set
    End Property

    Private mClientName As String
    Public Property ClientName() As String
        Get
            mClientName = Session("Client")
            Return mClientName
        End Get
        Set(ByVal value As String)
            mClientName = value
            Session("Client") = mClientName
        End Set
    End Property

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Me.ClientName = Request.QueryString("Client")
            FillDept()
        End If
    End Sub

    Public Sub FillDept()
        Dim deptGW As New DeptGateway
        dropDownDept.Items.Add(New ListItem("Select Dept", "0"))
        deptGW.LoadAll()
        dropDownDept.DataSource = deptGW.Table
        dropDownDept.DataBind()
    End Sub

    Public Sub FillDeptPosition()
        ClearMsg()
        Dim deptPositionGW As New DeptPositionGateway
        deptPositionGW.LoadWhere("DeptID = " & Me.dropDownDept.SelectedValue)
        dropDownDeptPosition.DataSource = deptPositionGW.Table
        dropDownDeptPosition.DataBind()
    End Sub

    Protected Sub InsertDefault(ByVal sender As Object, ByVal e As CommandEventArgs)
        Dim deptID As String = dropDownDept.SelectedValue
        Dim deptPositionID As String = dropDownDeptPosition.SelectedValue
        If Not DeptPosExists(Me.ClientName, deptPositionID) Then
            Dim staffAssignmentMgr As New StaffAssignmentMgr
            staffAssignmentMgr.InsertDefault(Me.ClientName, deptID, deptPositionID)
            Me.AssignmentMgr.LoadByClientID(Me.ClientName)
            Page_UnLoad()
        Else
            AlreadyExistMsg()
        End If
    End Sub

    Public Sub ClearMsg()
        lblInsertNote.Visible = False
        lblInsertNote.Text = ""
    End Sub

    Private Sub AlreadyExistMsg()
        lblInsertNote.Visible = True
        lblInsertNote.Text = "Department Position Already Exists"
    End Sub

    Private Function DeptPosExists(ByVal Client As String, ByVal DeptPositionID As Integer) As Boolean
        Return Me.AssignmentMgr.CheckStaffAssignmentsForExistingPosition(Client, DeptPositionID)
    End Function

    Protected Sub Page_UnLoad()
        Dim cmdTxt As String = "AssignStaffMain.aspx?FromInsertDeptPos=Y&Client='" & " + escape(""" & Me.ClientName & """); window.close();"
        Dim scriptTxt As New StringBuilder()
        scriptTxt.Append("<script>")
        scriptTxt.Append("window.opener.location.href='")
        scriptTxt.Append(cmdTxt)
        scriptTxt.Append(";")
        scriptTxt.Append("</script>")
        ClientScript.RegisterStartupScript(Me.GetType(), "script", scriptTxt.ToString())
    End Sub

End Class