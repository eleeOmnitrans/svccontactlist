﻿Imports CrystalDecisions.CrystalReports.Engine

Partial Public Class PreviewSvcContactListRpt
    Inherits System.Web.UI.Page

    Public rptMgr As ReportMgr

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Buffer = False
        LoadCRRpt()
    End Sub

    ''' <summary>
    ''' Bind the loaded report to the crystal report viewer on the web page
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub LoadCRRpt()
        Dim AssignmentMgr As New StaffAssignmentMgr
        LoadClient(AssignmentMgr)
        rptMgr = New ReportMgr
        Dim onlyDisplayed = (From Dept In AssignmentMgr.Table.AsEnumerable _
                                    Where Dept!NotDisplayed = 0).ToList
		If onlyDisplayed.Count > 0 Then							
			Dim rptDocument As ReportDocument = rptMgr.GetReportLoadedByDT(onlyDisplayed) 'bind to report
			rptDocument.SetParameterValue("Company", Request.QueryString("Company"))
			rptDocument.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, False, "Service Contact List")
		END IF
    End Sub

    ''' <summary>
    ''' Load Client
    ''' </summary>
    ''' <param name="AssignmentMgr"></param>
    ''' <remarks></remarks>
    Private Sub LoadClient(ByRef AssignmentMgr As StaffAssignmentMgr)
        AssignmentMgr.Lang = Request.QueryString("Lang")
        AssignmentMgr.LoadByClientID(Request.QueryString("Client"))
    End Sub

    ''' <summary>
    ''' Unload all resources when page unloads
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_UnLoad(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        rptMgr.Dispose()
    End Sub
End Class