﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'     Runtime Version:2.0.50727.3649
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On



Partial Public Class InsertDeptPosition

    '''<summary>
    '''lblDeptPos control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblDeptPos As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''dropDownDept control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents dropDownDept As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''dropDownDeptPosition control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents dropDownDeptPosition As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''btnAdd control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnAdd As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnClose control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnClose As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''lblInsertNote control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblInsertNote As Global.System.Web.UI.WebControls.Label
End Class
