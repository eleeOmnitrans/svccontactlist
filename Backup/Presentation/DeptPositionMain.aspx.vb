﻿Public Partial Class DeptPositionMain
    Inherits System.Web.UI.Page

#Region "Class properties"
    Private mDeptPositionGW As DeptPositionGateway
    Public Property DeptPositionGW() As DeptPositionGateway
        Get
            mDeptPositionGW = Session("DeptPositionGW")
            Return mDeptPositionGW
        End Get
        Set(ByVal value As DeptPositionGateway)
            mDeptPositionGW = value
            Session("DeptPositionGW") = mDeptPositionGW
        End Set
    End Property

    Private mDeptGW As DeptGateway
    Public Property DeptGW() As DeptGateway
        Get
            Return mDeptGW
        End Get
        Set(ByVal value As DeptGateway)
            mDeptGW = value
        End Set
    End Property

    Private mRecIDCurrent As Integer
    Public Property RecIDCurrent() As Integer
        Get
            mRecIDCurrent = Session("RecIDCurrent")
            Return mRecIDCurrent
        End Get
        Set(ByVal value As Integer)
            mRecIDCurrent = value
            Session("RecIDCurrent") = mRecIDCurrent
        End Set
    End Property

    Private mDeptID As Integer
    Public Property DeptID() As Integer
        Get
            mDeptID = Session("DeptID")
            Return mDeptID
        End Get
        Set(ByVal value As Integer)
            mDeptID = value
            Session("DeptID") = mDeptID
        End Set
    End Property

    Private mCurrentDR As DataRow
    Public Property CurrentDR() As DataRow
        Get
            mCurrentDR = Session("CurrentDR")
            Return mCurrentDR
        End Get
        Set(ByVal value As DataRow)
            mCurrentDR = value
            Session("CurrentDR") = mCurrentDR
        End Set
    End Property


#End Region


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            '  If (Request.QueryString("IsDataLoaded") = "False") Or (Request.QueryString("IsDataLoaded") = "") Then
            btnAddPosition.Visible = False
            FillDeptDropDown()
            Me.DeptPositionGW = New DeptPositionGateway()
            'End If
        End If
    End Sub

    Private Sub FillDeptDropDown()
        Me.DeptGW = New DeptGateway()
        Me.DeptGW.LoadAll()
        Dept.DataSource = Me.DeptGW.Table
        Dept.DataBind()
        Dept.Items.Insert(0, New ListItem("Select a Department", "Select a Department"))
        Dept.SelectedIndex = 0
    End Sub
    ''' <summary>
    ''' Display gridview using datatable values
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub BindData()
        GridView1.DataSource = Me.DeptPositionGW.Table
        GridView1.DataBind()
    End Sub

    Private Sub RefreshAndBindData()
        Me.DeptPositionGW.LoadWhere("DeptID = " & Me.DeptID.ToString & " Order By DisplayOrder ASC ")
        BindData()
    End Sub

    Protected Sub ChangeDept(ByVal sender As Object, ByVal e As EventArgs) Handles Dept.SelectedIndexChanged
        Me.DeptID = Dept.SelectedIndex
        Me.DeptPositionGW = Session("DeptPositionGW")
        RefreshAndBindData()
        btnAddPosition.Visible = True
    End Sub

    ''' <summary>
    ''' Gets triggered when Update link clicked in edit mode
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub GridView1_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles GridView1.RowUpdating
        UpdateRow(e.RowIndex)
    End Sub

    Sub UpdateRow(ByVal currRowIndex As Integer)
        UpdateDataTableWithGridValues(currRowIndex)
        RefreshAndBindData() 'Bind data to the GridView control.
    End Sub

    ''' <summary>
    ''' Send updated gridview row to data table and commit to db
    ''' </summary>
    ''' <param name="RowIndex"></param>
    ''' <remarks></remarks>
    Private Sub UpdateDataTableWithGridValues(ByVal RowIndex As Integer)
        Dim gridRow As GridViewRow = GridView1.Rows(RowIndex)
        Dim recID As Integer = GridView1.DataKeys(RowIndex).Value.ToString() 'DataKey avoids having to create a boundfield on the id.  Boundfield would have to be readonly=false to be able to access'
        If recID < 0 Then
            InsertRefreshDeptPositionGW(recID)
        End If
        Dim acutalRowIndex = gridRow.DataItemIndex 'dataItemIndex will not equal RowIndex if using paging
        'Update the values.  Have to use DataItemIndex to get row count.  (eg if row 10 is first item on page 2, Rowindex will = 0, and DataItemIndex will = 10
        GridViewToDataTableValMapper(recID, gridRow) ', dg.table.Rows(acutalRowIndex))
        GridView1.EditIndex = -1 'Reset the edit index.  The zero-based index of the row to edit. The default is -1, which indicates that no row is being edited.
    End Sub

    Private Sub InsertRefreshDeptPositionGW(ByRef recID As Integer)
        recID = Me.DeptPositionGW.Insert(Me.CurrentDR)
        Me.DeptID = Session("DeptID")
        RefreshAndBindData()
    End Sub

    ''' <summary>
    ''' Overwrite datatable row items values with gridview values.  Commit to db
    ''' </summary>
    ''' <param name="RecID"></param>
    ''' <param name="GridRow"></param>
    ''' <remarks></remarks>
    Private Sub GridViewToDataTableValMapper(ByVal RecID As Integer, ByVal GridRow As GridViewRow)
        Dim DTRow As DataRow = Me.DeptPositionGW.RowByKey(RecID, "ID")
        Dim tbPositionDesc As TextBox = GridRow.FindControl("PositionDesc")
        Dim tbSubDeptName As TextBox = GridRow.FindControl("SubDeptName")
        Dim tbPositionDescFR As TextBox = GridRow.FindControl("PositionDescFR")
        Dim tbSubDeptNameFR As TextBox = GridRow.FindControl("SubDeptNameFR")
        Dim tbDisplayOrder As TextBox = GridRow.FindControl("DisplayOrder")
        Dim cbActive As CheckBox = GridRow.FindControl("Active")

        With DTRow
            .Item("PositionDesc") = tbPositionDesc.Text
            .Item("SubDeptName") = tbSubDeptName.Text
            .Item("PositionDescFR") = tbPositionDescFR.Text
            .Item("SubDeptNameFR") = tbSubDeptNameFR.Text
            .Item("DisplayOrder") = tbDisplayOrder.Text
            .Item("LastChanged") = Now()
            .Item("Active") = cbActive.Checked
        End With
        Me.DeptPositionGW.Update(DTRow)
    End Sub

    Protected Sub GridView1_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles GridView1.RowEditing
        EditRow(e.NewEditIndex)
    End Sub

    Sub EditRow(ByVal currRowIndex As Integer)
        GridView1.EditIndex = currRowIndex   'set index number of row being edited.  NewEditIndex evaluates to index of row being edited. 
        SetRecIDCurrent(currRowIndex)   'set current ID as session var
        BindData() 'Bind data to the GridView control.
    End Sub

    ''' <summary>
    ''' Set RecID session var
    ''' </summary>
    ''' <param name="RowIndex"></param>
    ''' <remarks></remarks>
    Private Sub SetRecIDCurrent(ByVal RowIndex As Integer)
        Me.RecIDCurrent = GridView1.DataKeys(RowIndex).Value.ToString()
    End Sub

    Protected Sub GridView1_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles GridView1.RowCancelingEdit
        CancelEditRow()
    End Sub

    Sub CancelEditRow()
        GridView1.EditIndex = -1 'Reset the edit index.
        RefreshAndBindData() 'Bind data to the GridView control.
    End Sub

    Protected Sub btnAddPosition_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddPosition.Click
        AddDeptPositionProcess()
    End Sub

    Private Sub AddDeptPositionProcess()
        AddDeptPosition()
        EditNewDeptPosition()
    End Sub

    Private Sub AddDeptPosition()
        Dim DTRow As DataRow = Me.DeptPositionGW.Table.NewRow
        With DTRow
            .Item("PositionDesc") = ""
            .Item("SubDeptName") = ""
            .Item("PositionDescFR") = ""
            .Item("SubDeptNameFR") = ""
            .Item("DisplayOrder") = 0
            .Item("DeptID") = Me.DeptID.ToString
            .Item("LastChanged") = Now()
            .Item("Active") = 1
        End With
        Me.DeptPositionGW.Table.Rows.Add(DTRow)
        Me.CurrentDR = DTRow
    End Sub

    Private Sub EditNewDeptPosition()
        Dim currRowIndex As Integer = GridView1.Rows.Count
        GridView1.EditIndex = currRowIndex
        BindData()
    End Sub

End Class