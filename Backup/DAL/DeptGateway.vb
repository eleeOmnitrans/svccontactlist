﻿Imports System.Data.SqlClient

Public Class DeptGateway
    Inherits DataGatewayBase

    Sub New()
        MyBase.TableName = "Dept"
        MyBase.mConnectStr = GetAppDBConnectStr()
    End Sub

    ''' <summary>
    ''' set to typed data table or non-typed data table
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Overrides Function newDataTable() As DataTable
        Return New Dept.DeptDataTable
    End Function

#Region "Class Methods"

    'Sub LoadDeptByID(ByVal WhereClause As String)
    '    Me.LoadWhere(WhereClause)
    'End Sub

    ''' <summary>
    ''' Insert records (from datable) to database.
    ''' </summary>
    Public Function Insert() As Integer

        Dim retval As Integer = 0
        Dim retDeptID As New SqlParameter("@ID", SqlDbType.Int)
        Const SPROCNAME As String = "Dept_Insert"
        Const FIRSTROW As Integer = 0

        With Me.Table.Rows(FIRSTROW)
            For Each dr As DataRow In Me.Table.Rows
                Dim sqlCmd As New SqlCommand(SPROCNAME)

                sqlCmd.Parameters.Add(retDeptID)
                retDeptID.Direction = ParameterDirection.Output
                sqlCmd.Parameters.AddWithValue("@Name", .Item("Name"))
                sqlCmd.Parameters.AddWithValue("@DisplayOrder", .Item("DisplayOrder"))
                sqlCmd.Parameters.AddWithValue("@LastChanged", .Item("LastChanged"))

                ExecSQLSProcHelper(sqlCmd)
                retval = CInt(retDeptID.Value)
                sqlCmd.Dispose()

            Next
        End With
        Return retval
    End Function

    Public Sub Update()
        Const SPROCNAME As String = "Dept_Update"
        Dim sqlCmd As New SqlCommand(SPROCNAME)
        For Each dr As DataRow In Me.Table.Rows
            With dr
                sqlCmd.Parameters.AddWithValue("@ID", .Item("ID"))
                sqlCmd.Parameters.AddWithValue("@Name", .Item("Name"))
                sqlCmd.Parameters.AddWithValue("@DisplayOrder", .Item("DisplayOrder"))
                sqlCmd.Parameters.AddWithValue("@LastChanged", .Item("LastChanged"))
            End With
            ExecSQLSProcHelper(sqlCmd)
        Next
    End Sub

    Public Sub UpdateByID(ByVal SqlTxt As String)
        Dim sqlCmd As New SqlCommand(SqlTxt)
        ExecSQLCmdTxtHelper(sqlCmd)
    End Sub

#End Region

#Region "DataGateway code"
#End Region

#Region "Dataset Holder Code"

#End Region

#Region "TestCode"
#End Region

#Region "Code No Longer Used"
#End Region


End Class
