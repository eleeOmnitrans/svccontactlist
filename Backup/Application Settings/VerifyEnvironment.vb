﻿Public Class VerifyEnvironment
#Region "Exception Handling VerificationRoutines - Do not delete"
    ''' <summary>
    ''' Forces division by zero exception.  Use to verify if top level application exception handling is working
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub testDivByZeroWithoutTryCatch()
        Dim n, d, x As Integer
        n = 100
        d = 0
        x = n / d
        'MsgBox("Proceeding to run invalid math operation.  Will cause unhandled exception (because no try catch block in code).  Expects Top Level exception handling routines to catch and log error")
        'MsgBox("Divide by zero {0}", n / d) ' will throw error
    End Sub

    ''' <summary>
    ''' Forces division by zero exception.  Use to verify if handled exception policy is working
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub testDivByZeroWithManagedTryCatch()
        Dim n, d As Integer
        n = 100
        d = 0
        Try
            MsgBox("Proceeding to run invalid math operation.  Will cause unhandled exception (because no try catch block in code).  Expects Top Level exception handling routines to catch and log error")
            MsgBox("Divide by zero {0}", n / d) ' will throw error
        Catch ex As Exception
            'Example: catch locally (instead of top level app exception handling).  Allows you to handle exception, eg close connection
            Dim rethrow As Boolean
            rethrow = ExceptionPolicy.HandleException(ex, EXPOLICY)
            If (rethrow) Then Throw
        Finally
            MsgBox("In Finally segment of try/catch from managed exception")
            'Note: if no significant code in catch or finally block, consider not using try block and let error bubble up to top level exception handling
        End Try
    End Sub

    Public Sub MsgBoxTestConfigFiles()
        MsgBox(String.Format("TestSettingNameInAppconfig key value from app.config = {0}", System.Configuration.ConfigurationManager.AppSettings("TestSettingNameInAppconfig")))
        MsgBox(String.Format("TestSettingNameInAppconfig key value from app.config = {0}", System.Configuration.ConfigurationManager.AppSettings("TestSettingNameInCommonConfig")))
        MsgBox(String.Format("dbconnect = {0}", System.Configuration.ConfigurationManager.AppSettings("DBConnectStr")))
    End Sub

    Public Function GetConfigValue(ByVal KeyVal As String) As String
        Return System.Configuration.ConfigurationManager.AppSettings(KeyVal)
    End Function

    Public Function GetWebConfigValue(ByVal KeyVal As String) As String
        Return System.Web.Configuration.WebConfigurationManager.AppSettings(KeyVal)
    End Function

#End Region
End Class
