﻿Imports System.Data.SqlClient

Public Class DeptPositionGateway
    Inherits DataGatewayBase

    Sub New()
        MyBase.TableName = "DeptPosition"
        MyBase.mConnectStr = GetAppDBConnectStr()
    End Sub

    ''' <summary>
    ''' set to typed data table or non-typed data table
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Overrides Function newDataTable() As DataTable
        Return New DeptPosition.DeptPositionDataTable
    End Function

#Region "Class Methods"

    'Sub LoadDeptPositionByID(ByVal WhereClause As String)
    '    Me.LoadWhere(WhereClause)
    'End Sub

    ''' <summary>
    ''' Insert records (from datable) to database.
    ''' </summary>
    Public Function Insert(ByVal DR As DataRow) As Integer

        Dim retval As Integer = 0
        Dim retDeptPositionID As New SqlParameter("@ID", SqlDbType.Int)
        Const SPROCNAME As String = "DeptPosition_Insert"
        ' Const FIRSTROW As Integer = 0

        'With Me.Table.Rows(FIRSTROW)
        '    For Each dr As DataRow In Me.Table.Rows
        Dim sqlCmd As New SqlCommand(SPROCNAME)

        With DR
            sqlCmd.Parameters.Add(retDeptPositionID)
            retDeptPositionID.Direction = ParameterDirection.Output
            sqlCmd.Parameters.AddWithValue("@PositionDesc", .Item("PositionDesc"))
            sqlCmd.Parameters.AddWithValue("@SubDeptName", .Item("SubDeptName"))
            sqlCmd.Parameters.AddWithValue("@PositionDescFR", .Item("PositionDescFR"))
            sqlCmd.Parameters.AddWithValue("@SubDeptNameFR", .Item("SubDeptNameFR"))
            sqlCmd.Parameters.AddWithValue("@DeptID", .Item("DeptID"))
            sqlCmd.Parameters.AddWithValue("@DisplayOrder", .Item("DisplayOrder"))
            sqlCmd.Parameters.AddWithValue("@LastChanged", .Item("LastChanged"))
            sqlCmd.Parameters.AddWithValue("@Active", .Item("Active"))
            ExecSQLSProcHelper(sqlCmd)
            retval = CInt(retDeptPositionID.Value)
            sqlCmd.Dispose()

            'Next
        End With
        Return retval
    End Function

    Public Sub Update(ByVal DTRow As DataRow)
        Const SPROCNAME As String = "DeptPosition_Update"
        Dim sqlCmd As New SqlCommand(SPROCNAME)
        ' For Each dr As DataRow In Me.Table.Rows
        With DTRow 'dr
            sqlCmd.Parameters.AddWithValue("@ID", .Item("ID"))
            sqlCmd.Parameters.AddWithValue("@PositionDesc", .Item("PositionDesc"))
            sqlCmd.Parameters.AddWithValue("@SubDeptName", .Item("SubDeptName"))
            sqlCmd.Parameters.AddWithValue("@PositionDescFR", .Item("PositionDescFR"))
            sqlCmd.Parameters.AddWithValue("@SubDeptNameFR", .Item("SubDeptNameFR"))
            ' sqlCmd.Parameters.AddWithValue("@DeptID", .Item("DeptID"))
            sqlCmd.Parameters.AddWithValue("@DisplayOrder", .Item("DisplayOrder"))
            sqlCmd.Parameters.AddWithValue("@LastChanged", .Item("LastChanged"))
            sqlCmd.Parameters.AddWithValue("@Active", .Item("Active"))
        End With
        ExecSQLSProcHelper(sqlCmd)
        ' Next
    End Sub

    Public Sub UpdateByID(ByVal SqlTxt As String)
        Dim sqlCmd As New SqlCommand(SqlTxt)
        ExecSQLCmdTxtHelper(sqlCmd)
    End Sub

#End Region

End Class
