﻿Imports System.Data.SqlClient
Imports System.Data.Common

''' <summary>
''' Manages all database interactions.  Uses Typed datasets as data transfer objects to exchange data between biz and data layer.
''' Abstract class.  Can't be instantiated directly.
''' </summary>

Public MustInherit Class DataGatewayBase

    'Public mTableNames As New List(Of String)
    Public TableName As String
    Public mDS As New DataSet
    Public mConnectStr As String

    ''' <summary>
    ''' 2011/09/06: must be implemented by derived class to return the proper data table type
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected MustOverride Function newDataTable() As DataTable


#Region "Class methods"

    ''' <summary>
    ''' return main dataset of data gateway
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function DS() As System.Data.DataSet
        Return Me.mDS
    End Function

    ''' <summary>
    ''' return main data table of data gateway
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Table() As DataTable
        Return Me.DS.Tables(TableName)
    End Function

    ''' <summary>
    ''' return connection string for data gateway
    ''' </summary>
    ''' <param name="Connstr"></param>
    ''' <remarks></remarks>
    Public Sub ConnectionStr(ByVal Connstr As String)
        Me.mConnectStr = Connstr
    End Sub

    ''' <summary>
    ''' return numbers rows in primary data table
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function RowCount() As Integer
        Dim rows As Integer = 0

        If DS.Tables.Contains(TableName) Then
            rows = Me.Table.Rows.Count
        End If
        Return rows
    End Function
    ''' <summary>
    ''' add new primary blank table.  Used for data entry instead of fetching from db
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub AddNew()
        Me.DropTableIfAlreadyExists() ' added as of Feb 1, 2012 
        Me.DS.Tables.Add(newDataTable)
    End Sub


    ''' <summary>
    ''' Load all records in to data table
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub LoadAll()
        Dim cmdStr As String = String.Format("select * from {0}", Me.TableName)
        Me.GoFill(cmdStr)
    End Sub


    ''' <summary>
    ''' Load all records in to data table according to Sort order
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub LoadAll(ByVal SortStr As String)
        Dim cmdStr As String = String.Format("select * from {0} order by {1}", Me.TableName, SortStr)
        Me.GoFill(cmdStr)
    End Sub


    ''' <summary>
    ''' Load all records in to data table for given WhereClause
    ''' </summary>
    ''' <param name="WhereClause"></param>
    ''' <remarks></remarks>
    Public Sub LoadWhere(ByVal WhereClause As String)
        Dim cmdStr As String = String.Format("select * from {0} where {1} ", Me.TableName, WhereClause)
        Me.GoFill(cmdStr)
    End Sub

    ''' <summary>
    ''' Load primary data table using sql storeproc named SprocName
    ''' </summary>
    ''' <param name="SprocName">Name of storedproc to execute for fill</param>
    ''' <remarks></remarks>
    Public Sub LoadWithSProc(ByVal SprocName As String)
        Dim command As New SqlCommand(SprocName)
        command.CommandType = CommandType.StoredProcedure

        GoFill(command)
    End Sub

    Public Sub LoadWhere(ByVal Command As SqlCommand)
        Command.CommandType = CommandType.StoredProcedure
        GoFill(Command)
    End Sub

    ''' <summary>
    ''' Execute fill statement and add resulting data table to primary data set
    ''' </summary>
    ''' <param name="SqlStr"></param>
    ''' <remarks>overload for GoFill.  CmdType defaults to .Text</remarks>
    Private Sub GoFill(ByVal SqlStr As String)
        Dim command As New SqlCommand(SqlStr)
        command.CommandType = CommandType.Text

        GoFill(command)
    End Sub

    ''' <summary>
    ''' Execute fill statement and add resulting data table to primary data set
    ''' expects a Command.CommandText, and any relevant Command.Pparameters
    ''' has to be set ahead of time 
    ''' </summary>
    ''' <param name="Command">SQL Command Object to execute</param>
    ''' <remarks></remarks>
    Private Sub GoFill(ByVal Command As SqlCommand)
        Dim connection As SqlConnection = New SqlConnection(Me.mConnectStr)
        Dim adapter As SqlDataAdapter
        Dim dt As DataTable = newDataTable()

        DropTableIfAlreadyExists()  'if table already exists, drop it before adding new results
        Command.Connection = connection

        Try
            If connection.State = ConnectionState.Closed Then
                connection.Open()
            End If

            Command.ExecuteNonQuery()
            adapter = New SqlDataAdapter(Command)
            adapter.Fill(dt)
            mDS.Tables.Add(dt)

        Catch ex As Exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, EXPOLICY) 'write to exception log
            Logger.Write(ex.Message, LOGPOLICY_DATAACCESS) 'Write to activity log
            If (rethrow) Then Throw
        Finally
            connection.Close()
        End Try
    End Sub

    Private Sub DropTableIfAlreadyExists()
        If (DS.Tables.Contains(Me.TableName)) Then
            DS.Tables.Remove(Me.TableName)
        End If
    End Sub

    ''' <summary>
    ''' Execute SQL Stored Proc given Command Object
    ''' CommandObj.CommandText must be set, and any relevant CommandObj.Parameters
    ''' Helper will handle connection and exception handling
    ''' </summary>
    ''' <param name="commandobj"></param>
    ''' <remarks></remarks>
    Private Sub ExecSQLHelper(ByVal CommandObj As SqlCommand, ByVal CmdType As System.Data.CommandType)

        Dim connection As SqlConnection = New SqlConnection(Me.mConnectStr)
        CommandObj.Connection = connection
        CommandObj.CommandType = CmdType

        Try
            connection.Open()
            CommandObj.ExecuteNonQuery()

        Catch ex As Exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, EXPOLICY) 'write to exception log
            Logger.Write(ex.Message, LOGPOLICY_DATAACCESS) 'Write to activity log
            If (rethrow) Then Throw
        Finally
            connection.Close()
        End Try
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="CommandObj"></param>
    ''' <remarks></remarks>
    Protected Sub ExecSQLSProcHelper(ByVal CommandObj As SqlCommand)
        ExecSQLHelper(CommandObj, CommandType.StoredProcedure)
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="CommandObj"></param>
    ''' <remarks></remarks>
    Protected Sub ExecSQLCmdTxtHelper(ByVal CommandObj As SqlCommand)
        ExecSQLHelper(CommandObj, CommandType.Text)
    End Sub


    ''' <summary>
    ''' Return data row from primary data table with row count = RowIndex
    ''' </summary>
    ''' <param name="RowIndex">ordinal row value to return</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Row(ByVal RowIndex As Integer) As DataRow
        Return Table.Rows(RowIndex)
    End Function

    ''' <summary>
    ''' return value of field named Fieldname as object
    ''' </summary>
    ''' <param name="RowIndex"></param>
    ''' <param name="Fieldname"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function RowVal(ByVal RowIndex As Integer, ByVal Fieldname As String) As String
        Return Table.Rows(RowIndex).Item(Fieldname).ToString
    End Function

    ''' <summary>
    ''' Return 1st row where value of KeyFieldName = Key
    ''' </summary>
    ''' <param name="Key"></param>
    ''' <param name="KeyFieldName"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function RowByKey(ByVal Key As Long, ByVal KeyFieldName As String) As DataRow
        Const RETURNFIRSTROW As Integer = 0
        Dim criteria As String = String.Format("{0} = {1}", KeyFieldName, Key)
        Dim j As DataRow()
        j = Table.Select(criteria)
        Return j(RETURNFIRSTROW)
    End Function

    ''' <summary>
    ''' Return value of Fieldname, from 1st row where value of KeyFieldName = Key
    ''' </summary>
    ''' <param name="Key"></param>
    ''' <param name="KeyFieldName"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function RowValByKey(ByVal Key As Long, ByVal KeyFieldName As String, ByVal FieldName As String) As Object
        Return RowByKey(Key, KeyFieldName).Item(FieldName)
    End Function

    Public Sub ConsoleShowTable()
        ConsoleShowTable(TableName)
    End Sub

    Public Sub ConsoleShowTable(ByVal TName)
        For Each i As DataRow In Me.DS.Tables(TName).Rows
            Console.WriteLine(String.Format("{0},{1},{2}", i.Item(0), i.Item(1), i.Item(2)))
        Next
    End Sub


    Public Sub ConsoleShowTableField(ByVal FldName As String)
        ConsoleShowTableField(FldName, TableName)
    End Sub

    Public Sub ConsoleShowTableField(ByVal FldName As String, ByVal TName As String)
        For Each i As DataRow In Me.DS.Tables(TName).Rows
            Console.WriteLine(String.Format("{0}:{1}", FldName, i.Item(FldName)))
        Next
    End Sub


#End Region

#Region "TestCode"
#End Region

#Region "Code No Longer Used"
#End Region

End Class




