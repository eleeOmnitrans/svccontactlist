﻿Imports System.Data.SqlClient

Public Class ClientGateway
    Inherits DataGatewayBase

    Sub New()
        MyBase.TableName = "Client"
        MyBase.mConnectStr = GetAppDBConnectStr2()
    End Sub

    ''' <summary>
    ''' set to typed data table or non-typed data table
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Overrides Function newDataTable() As DataTable
        Return New Client.ClientDataTable
    End Function

#Region "Class Methods"

    'Sub LoadClientByID(ByVal WhereClause As String)
    '    Me.LoadWhere(WhereClause)
    'End Sub
#End Region

#Region "DataGateway code"
#End Region

#Region "Dataset Holder Code"

#End Region

#Region "TestCode"
#End Region

#Region "Code No Longer Used"
#End Region

End Class
