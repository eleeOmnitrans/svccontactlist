﻿<%@ Page Language="vb" AutoEventWireup="false"  EnableViewState="True" CodeBehind="Default.aspx.vb" Inherits="SvcContactListWeb._Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Service Contact List Web Facility</title>
</head>
<body>
    <form id="ContactForm" runat="server">
    <table align="right">
       <tr>
        <td align="right">Search:</td>
        <td align="left">
            <asp:TextBox ID="myTextBox" autocomplete ="off" runat="server" 
                style="margin-left: 0px" Height="21px" Width="346px"></asp:TextBox>
        </td>
        <br /> 
        <td align="right"><asp:Button ID="btnFind" runat="server" Text="Find" /></td>
      </tr>
      <tr>
         <td align="right"></td>
         <td align="left">
            <asp:DropDownList ID="ClientList" runat="server" OnSelectedIndexChanged="ClientList_SelectedIndexChanged"
             AutoPostBack="True" style="margin-left: 0px" Height="21px" Width="352px"></asp:DropDownList>
         </td>
      </tr>
    </table>
    <br />
    Client MINATTGE<asp:TextBox ID="ClientID2" runat="server" Text="ABC"></asp:TextBox>
    <br />
 <%--   <asp:DropDownList ID="Clientddl" runat="server">
    </asp:DropDownList>--%>
    <br />
    <br />
    <br />
    <asp:Table ID="tblContact" BorderWidth="2" runat="server">
    </asp:Table>
    <br />
    <asp:Button ID="btnEditMode" runat="server" Text="Edit" /> &nbsp; &nbsp;
    <asp:Button ID="Save" runat="server" Text="Save" /> &nbsp; &nbsp;
    <asp:Button ID="btnTest" runat="server" Text="Preview Rpt Test" OnClientClick="window.open('http://localhost:4115/Presentation/PreviewSvcContactListRpt.aspx?ClientID=' + ContactForm.ClientID2.value, 'PreviewRpt', 'resizable=yes, scrollbars=yes, titlebar=yes, width=900, height=900, top=10, left=10') ;" />
    </form>
    
    
        <%--OnClientClick="window.open('', 'PreviewRpt', 'resizable=yes, scrollbars=yes, titlebar=yes, width=800, height=900, top=10, left=10') ;" />--%>
       <%-- OnClientClick="ContactForm.target ='_blank';" OnClick="btnTest_Click"  />--%>
    
</body>
</html>
