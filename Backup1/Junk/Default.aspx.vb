﻿'Imports System.Data
'Imports System.Data.SqlClient

'Partial Public Class _Default
'    Inherits System.Web.UI.Page

'#Region "Class Properties"

'    Private mDeptGW As DeptGateway
'    Public Property DeptGW() As DeptGateway
'        Get
'            Return mDeptGW
'        End Get
'        Set(ByVal value As DeptGateway)
'            mDeptGW = value
'        End Set
'    End Property

'    Private mDeptPositionGW As DeptPositionGateway
'    Public Property DeptPositionGW() As DeptPositionGateway
'        Get
'            Return mDeptPositionGW
'        End Get
'        Set(ByVal value As DeptPositionGateway)
'            mDeptPositionGW = value
'        End Set
'    End Property

'    Private mStaffGW As StaffGateway
'    Public Property StaffGW() As StaffGateway
'        Get
'            Return mStaffGW
'        End Get
'        Set(ByVal value As StaffGateway)
'            mStaffGW = value
'        End Set
'    End Property

'    Private mStaffAssignmentsGW As StaffAssignmentsGateway
'    Public Property StaffAssignmentsGW() As StaffAssignmentsGateway
'        Get
'            Return mStaffAssignmentsGW
'        End Get
'        Set(ByVal value As StaffAssignmentsGateway)
'            mStaffAssignmentsGW = value
'        End Set
'    End Property

'    Private mClientName As String
'    Public Property ClientName() As String
'        Get
'            Return mClientName
'        End Get
'        Set(ByVal value As String)
'            mClientName = value
'        End Set
'    End Property


'    Private EditMode As Boolean
'    Private ClientStatus As Boolean

'#End Region

'#Region "Class Methods"

'    Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
'        'GoMain()
'    End Sub

'    Sub GoMain()
'        SetEditMode()
'        LoadGWs()
'        InsertTableRowsForEachDept()
'    End Sub

'    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
'        If Not Page.IsPostBack Then
'            '    If (ViewState("ddlPositionClientServiceRepresentativeOcean1") <> "") Then
'            '        MsgBox(ViewState("ddlPositionClientServiceRepresentativeOcean1").ToString)ww
'            '    End If
'            ' create a temp session gw to restore all ddl 

'            GoMain()
'            ClientList.Visible = False
'        Else
'            'Me.DeptGW = Session("DeptGW")
'            'Me.StaffGW = Session("StaffGW")
'            'Me.DeptPositionGW = Session("DeptPositionGW")
'            Me.StaffAssignmentsGW = Session("StaffAssignmentsGW")
'            Me.StaffGW = Session("StaffGW")

'            SetEditMode()


'            InsertTableRowsForEachDept()
'        End If

'        'not in right place
'        'check for existing client on staffassignment table, set from the client dropdownlist 


'        'If (CheckClientExists(8888989)) Then
'        '    Me.ClientStatus = True
'        'Else
'        '    Me.ClientStatus = False
'        'End If
'    End Sub

'    Sub SetEditMode()
'        If (Request.QueryString("edit") = "True") Then
'            Me.EditMode = True
'        Else
'            Me.EditMode = False
'        End If
'    End Sub

'    Sub LoadGWs()
'        LoadStaffGW()
'        'LoadDeptGW()
'        'LoadDeptPositionGW()
'        LoadStaffAssignmentsGW()
'        'LoadClientGW()
'    End Sub

'    Sub LoadStaffGW()
'        Me.StaffGW = New StaffGateway
'        Me.StaffGW.LoadAll()
'        Session("StaffGW") = Me.StaffGW
'    End Sub

'    'Sub LoadDeptGW()
'    '    Me.DeptGW = New DeptGateway
'    '    Me.DeptGW.LoadAll("DisplayOrder ASC")
'    '    Session("DeptGW") = Me.DeptGW
'    'End Sub

'    'Sub LoadDeptPositionGW()
'    '    Me.DeptPositionGW = New DeptPositionGateway
'    '    Me.DeptPositionGW.LoadAll("DisplayOrder ASC")
'    '    Session("DeptPositionGW") = Me.DeptPositionGW
'    'End Sub

'    Sub LoadStaffAssignmentsGW()
'        Me.StaffAssignmentsGW = New StaffAssignmentsGateway
'        'Me.StaffAssignmentsGW.LoadAll()

'        Me.StaffAssignmentsGW.LoadExistingClient("ABC Inc")

'        Session("StaffAssignmentsGW") = Me.StaffAssignmentsGW
'    End Sub

'    'Sub LoadClientGW()
'    '    Dim ClientGW As New ClientGateway
'    '    ClientGW.LoadWhere("name1='ABC'")
'    'End Sub

'    'Sub InsertTableRowsForEachDept()
'    '    Dim deptCriteria As String = ""
'    '    For Each dept In Me.DeptGW.Table.Rows
'    '        deptCriteria = "DeptID = " & dept.item("ID")
'    '        AddDeptRowHeaderInTable(dept.item("Name").ToString)
'    '        For Each positionDesc In Me.DeptPositionGW.Table.Select(deptCriteria)
'    '            Dim tr As New TableRow()
'    '            tr.Cells.Add(BuildPositionDescCell(positionDesc.Item("PositionDesc"), positionDesc.Item("id")))
'    '            tr.Cells.Add(BuildPositionDescandStaffAssignedCells(dept.item("id"), positionDesc.Item("id")))
'    '            tblContact.Rows.Add(tr)
'    '        Next
'    '    Next
'    'End Sub


'    Sub InsertTableRowsForEachDept()
'        Dim deptCriteria As String = ""
'        '   For Each dept In Me.StaffAssignmentsGW.Table.Rows
'        'deptCriteria = "DeptID = " & Dept.item("DeptID")
'        'AddDeptRowHeaderInTable(Dept.item("Name").ToString)
'        For Each positionDesc In Me.StaffAssignmentsGW.Table.Rows
'            Dim tr As New TableRow()
'            tr.Cells.Add(BuildPositionDescCell(positionDesc.Item("PositionDesc"), positionDesc.Item("DeptPositionID")))
'            tr.Cells.Add(BuildPositionDescandStaffAssignedCells(positionDesc.item("DeptID"), positionDesc.Item("DeptPositionID"), positionDesc.Item("StaffFirstName") & " " & positionDesc.Item("StaffLastName"), positionDesc.Item("StaffID")))
'            tblContact.Rows.Add(tr)
'        Next
'        ' Next
'    End Sub

'    Function BuildPositionDescandStaffAssignedCells(ByVal DeptID As Integer, ByVal DeptPositionID As String, ByVal StaffName As String, ByVal StaffID As Integer) As TableCell
'        If Me.EditMode Then
'            Return BuildStaffAssignedDropDownCell(DeptID, DeptPositionID, StaffID)
'        Else
'            Return BuildStaffAssignedLabelCell(StaffName)
'        End If
'    End Function

'    Private Function BuildPositionDescCell(ByVal Description As String, ByVal PosID As Integer) As TableCell
'        Dim cell As New TableCell
'        Dim position As New Label
'        position.Text = Description
'        position.ID = PosID
'        cell.Controls.Add(position)
'        Return cell
'    End Function

'    Private Function BuildStaffAssignedLabelCell(ByVal StaffName As String) As TableCell
'        Dim cell As New TableCell
'        Dim position As New Label
'        position.Text = StaffName
'        cell.Controls.Add(position)
'        Return cell
'    End Function

'    Private Function BuildStaffAssignedDropDownCell(ByVal DeptID As Integer, ByVal DeptPositionID As String, ByVal StaffID As Integer) As TableCell
'        Dim cell As New TableCell
'        Dim ddlstaffAssigned As New DropDownList
'        ddlstaffAssigned = GetStaffByDept(DeptID, DeptPositionID, StaffID)
'        cell.Controls.Add(ddlstaffAssigned)
'        Return cell
'    End Function

'    Function CheckClientExists(ByVal ClientID As Integer) As Boolean
'        Me.StaffAssignmentsGW.Table.DefaultView.RowFilter = "ClientID = " & ClientID
'        If Me.StaffAssignmentsGW.Table.DefaultView.Count > 0 Then
'            Return True
'        Else
'            Return False ' - WIP set to edit mode
'        End If
'    End Function

'    Sub AddDeptRowHeaderInTable(ByVal DeptHeader As String)
'        Dim tr As New TableRow()
'        tr.Cells.Add(BuildDeptHeaderCell(DeptHeader))
'        tblContact.Rows.Add(tr)
'    End Sub

'    Private Function BuildDeptHeaderCell(ByVal DeptHeaderStr As String) As TableCell
'        Dim cell As New TableHeaderCell()
'        Dim deptHeader As New Label
'        deptHeader.Text = DeptHeaderStr
'        deptHeader.ID = DeptHeaderStr
'        cell.ColumnSpan = 2
'        cell.BackColor = Drawing.Color.LightGray
'        cell.Controls.Add(deptHeader)
'        Return cell
'    End Function

'    Function GetStaffByDept(ByVal DeptID As Integer, ByVal DeptPositionID As Integer, ByVal StaffID As Integer) As DropDownList
'        Dim ddlStaff As New DropDownList
'        ddlStaff.ID = "Position" & DeptPositionID.ToString
'        For Each staff In Me.StaffGW.Table.Select(String.Format("DeptID = {0}", DeptID))
'            ddlStaff.Items.Add(New ListItem(staff.Item("FirstName") & " " & staff.Item("LastName"), staff.Item("ID")))
'        Next
'        ddlStaff.SelectedIndex = ddlStaff.Items.IndexOf(ddlStaff.Items.FindByValue(StaffID))
'        Return ddlStaff
'    End Function

'    Protected Sub btnEditMode_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnEditMode.Click
'        Response.Redirect("default.aspx?edit=True")
'    End Sub

'    Protected Sub Save_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Save.Click
'        GoSave()
'    End Sub

'    Sub GoSave()
'        'Dim ddlList(27, 27) As String
'        'Dim loopCnt As Integer = 0
'        'GetddlValues(Me, ddlList, loopCnt)
'        'InsertToDB(ddlList)
'    End Sub

'    'Sub GetddlValues(ByVal Page As Control, ByRef ddlList(,) As String, ByRef loopCnt As Integer)
'    '    Const DEPTID As Integer = 0
'    '    Const STAFFID As Integer = 1
'    '    Const DEPTPOSITIONID As Integer = 2


'    '    For Each ctrl As Control In Page.Controls
'    '        If TypeOf ctrl Is DropDownList Then
'    '            ddlList(loopCnt, DEPTID) = 1 'CType(ctrl, DropDownList).Text
'    '            ddlList(loopCnt, STAFFID) = CType(ctrl, DropDownList).Text
'    '            ddlList(loopCnt, DEPTPOSITIONID) = CType(ctrl, DropDownList).ClientID
'    '            loopCnt += 1
'    '        Else
'    '            If ctrl.Controls.Count > 0 Then
'    '                GetddlValues(ctrl, ddlList, loopCnt)
'    '            End If
'    '        End If
'    '    Next
'    'End Sub

'    'Sub InsertToDB(ByVal ddlList(,) As String) 'mapper
'    '    Const DEPTID As Integer = 0
'    '    Const STAFFID As Integer = 1
'    '    Const DEPTPOSITIONID As Integer = 2



'    '    For i = 0 To 9 'ddlList.GetUpperBound(0) - 1
'    '        ' MsgBox(ddlList(i, STAFFID))

'    '        'Me.StaffAssignmentsGW.AddNew()
'    '        'Dim dr As DataRow = Me.StaffAssignmentsGW.Table.NewRow

'    '        'dr("ClientID") = 1
'    '        'dr("DeptPositionID") = ddlList(i, DEPTPOSITIONID)
'    '        'dr("StaffID") = ddlList(i, STAFFID)
'    '        'dr("DEPTID") = 1 'ddlList(i, DEPTID).ToString
'    '        'dr("LastChanged") = CDate("#2012/06/27 13:33:22#")

'    '        'Me.StaffAssignmentsGW.Table.Rows.Add(dr)
'    '        'Me.StaffAssignmentsGW.Insert()

'    '    Next
'    'End Sub

'#End Region


'    'Protected Sub btnTest_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnTest.Click
'    '    'Server.Transfer("\Presentation\PreviewSvcContactListRpt.aspx?ClientID=ABC")
'    '    ' Response.Redirect("http://localhost:4115/Presentation/PreviewSvcContactListRpt.aspx?ClientID=ABC")
'    'End Sub


'    Protected Sub btnFind_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnFind.Click
'        'Dim webService As New mtldevsvr1.ClientInfo
'        'webService.UseDefaultCredentials = True
'        'Dim list() As String
'        'list = webService.GetListBeginWithString(myTextBox.Text)
'        'ClientList.DataSource = list
'        'ClientList.DataBind()
'        'ClientList.Visible = True
'    End Sub

'    Protected Sub ClientList_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ClientList.SelectedIndexChanged
'        Me.myTextBox.Text = ClientList.Text
'        ClientList.Visible = False
'    End Sub
'End Class