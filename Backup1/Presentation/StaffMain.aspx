﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Site1.Master"  CodeBehind="StaffMain.aspx.vb"
    Inherits="SvcContactListWeb.StaffMain" %>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <br />
   <div style="font-family: Arial">
        Department 
         <asp:DropDownList ID="Dept" CssClass="style8" runat="server" DataTextField="Name"
            DataValueField="Name" OnSelectedIndexChanged="ChangeDept" AutoPostBack="True" />
        <br />
        <br />
        <br />
    <table align="left" border="0" cellpadding="0" cellspacing="0">
    <tr style="font-family: Arial"><td style="font-family: Arial">
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" AutoGenerateEditButton="True"
            DataKeyNames="ID" Style="font-family: Verdana; white-space: nowrap;" Font-Size="Small"
            CellPadding="8" Width="80%">
            <Columns>
                <asp:TemplateField Visible="true" HeaderStyle-Wrap="false" HeaderText="First Name">
                    <ItemStyle Wrap="False" CssClass="style7" />
                    <ItemTemplate>
                        <%#Eval("FirstName")%>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="FirstName" runat="server" Value='<%# Eval("FirstName").ToString()%>'>
                        </asp:TextBox>
                    </EditItemTemplate>
                </asp:TemplateField>
              
                <asp:TemplateField Visible="true" HeaderStyle-Wrap="false" HeaderText="Last Name">
                    <ItemStyle Wrap="False" CssClass="style7" />
                    <ItemTemplate>
                        <%#Eval("LastName")%>
                    </ItemTemplate>
                    <EditItemTemplate>
                     <asp:TextBox ID="LastName" runat="server"  
                         Value='<%# Eval("LastName").ToString()%>'>
                     </asp:TextBox>
                    </EditItemTemplate>
                </asp:TemplateField>
                                           
               <asp:TemplateField  Visible="true" HeaderText="Active"  >
                   <ItemStyle Wrap="False" CssClass="style7" />
                 <ItemTemplate >
                   <%#If(Eval("Active").ToString().Equals("True"), "Yes", "No")%>
                 </ItemTemplate>
                <EditItemTemplate>
                 <asp:CheckBox ID="Active" runat="server" Checked='<%# Convert.ToBoolean(Eval("Active")) %>'
                    Enabled="true" />
                </EditItemTemplate>
              </asp:TemplateField>
        
                         
                <asp:TemplateField Visible="true" HeaderText="Tel #">
                    <ControlStyle Width="90" />
                    <ItemStyle Wrap="False" CssClass="style7" />
                    <ItemTemplate>
                        <%#Eval("Tel")%>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="Tel"  runat="server"  Value='<%# Eval("Tel").ToString()%>'>
                        </asp:TextBox>
                    </EditItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField Visible="true" HeaderStyle-Wrap="false"  HeaderText="Tel Ext">
                    <ControlStyle Width="30" />
                    <ItemStyle Wrap="False" CssClass="style7" />
                    <ItemTemplate>
                        <%#Eval("TelExt")%>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="TelExt"  runat="server" 
                            Value='<%# Eval("TelExt").ToString()%>'>
                        </asp:TextBox>
                    </EditItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField Visible="true" HeaderText="Fax">
                    <ControlStyle Width="90" />
                    <ItemStyle Wrap="False" CssClass="style7" />
                    <ItemTemplate>
                        <%#Eval("Fax")%>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="Fax"  runat="server"  Value='<%# Eval("Fax").ToString()%>'>
                        </asp:TextBox>
                    </EditItemTemplate>
                </asp:TemplateField>
                
                
                <asp:TemplateField Visible="true" HeaderText="Email">
                    <ItemStyle Wrap="False" CssClass="style7" />
                    <ItemTemplate>
                        <%#Eval("Email")%>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="Email" runat="server" 
                            Value='<%# Eval("Email").ToString()%>'>
                        </asp:TextBox>
                    </EditItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField Visible="true" HeaderText="Cell">
                    <ControlStyle Width="90" />
                    <ItemStyle Wrap="False" CssClass="style7" />
                    <ItemTemplate>
                        <%#Eval("Cell")%>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="Cell"  runat="server" 
                            Value='<%# Eval("Cell").ToString()%>'>
                        </asp:TextBox>
                    </EditItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField Visible="true" HeaderText="Notes">
                    <ItemStyle Wrap="False" CssClass="style7" />
                    <ItemTemplate>
                        <%#Eval("Notes")%>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="Notes"  runat="server" 
                            Value='<%# Eval("Notes").ToString()%>'>
                        </asp:TextBox>
                    </EditItemTemplate>
                </asp:TemplateField>
            
            </Columns>
        </asp:GridView>
    </td>
    </tr>
        <tr><td align="right">
        <table width="100%" cellspacing="0" cellpadding="0" border="0"><tr>
        <td align="left">
            <asp:LinkButton ID="lbAssignStaffMainLink" CssClass="style8" PostBackUrl="AssignStaffMain.aspx"
                runat="server" Style="text-decoration: none"> Return to Main </asp:LinkButton>
        </td>
        <td align="right">
            <asp:Button ID="btnAddStaff" runat="server" Text="Add Staff" />
        </td>
        </tr></table>
        </td></tr>
        
    </table>
    </div>
 
</asp:Content> 