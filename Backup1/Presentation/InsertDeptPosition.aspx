﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site1.Master" CodeBehind="InsertDeptPosition.aspx.vb" Inherits="SvcContactListWeb.InsertDeptPosition" %>
<%--<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>--%>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <table>
        <tr>
            <td align="center">
                <asp:Label ID="lblDeptPos" Font-Bold="true" runat="server" Text="Add Deptment Position"></asp:Label>
                <br />
            </td>
        </tr>
        <tr>
            <td align="right">
                <table cellpadding="2" cellspacing="0">
                    <tr>
                        <td>
                            <asp:DropDownList ID="dropDownDept" OnSelectedIndexChanged="FillDeptPosition" DataValueField="ID"
                                DataTextField="Name" runat="server" AutoPostBack="True" AppendDataBoundItems="true" />
                                
                            <asp:DropDownList ID="dropDownDeptPosition" OnSelectedIndexChanged="ClearMsg" AutoPostBack="true"
                                DataValueField="ID" DataTextField="PositionDesc" runat="server" />
                                
                            <asp:Button ID="btnAdd" runat="server" Text="Insert" CommandName="InsertDefault"
                                OnCommand="InsertDefault" />
                        </td>
                        <td>
                            &nbsp;
                            <asp:Button ID="btnClose" runat="server" Text="Cancel" OnClientClick="javascript:window.close();" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblInsertNote" ForeColor="Red" visible="false" runat="server" Text=""></asp:Label>
            </td>
        </tr>
    </table>
   
</asp:Content>
