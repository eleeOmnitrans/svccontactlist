﻿Public Partial Class StaffMain
    Inherits System.Web.UI.Page

#Region "Class properties"
    Private mStaffGW As StaffGateway
    Public Property StaffGW() As StaffGateway
        Get
            mStaffGW = Session("StaffGW")
            Return mStaffGW
        End Get
        Set(ByVal value As StaffGateway)
            mStaffGW = value
            Session("StaffGW") = mStaffGW
        End Set
    End Property

    Private mDeptGW As DeptGateway
    Public Property DeptGW() As DeptGateway
        Get
            Return mDeptGW
        End Get
        Set(ByVal value As DeptGateway)
            mDeptGW = value
        End Set
    End Property

    Private mRecIDCurrent As Integer
    Public Property RecIDCurrent() As Integer
        Get
            mRecIDCurrent = Session("RecIDCurrent")
            Return mRecIDCurrent
        End Get
        Set(ByVal value As Integer)
            mRecIDCurrent = value
            Session("RecIDCurrent") = mRecIDCurrent
        End Set
    End Property

    Private mDeptID As Integer
    Public Property DeptID() As Integer
        Get
            mDeptID = Session("DeptID")
            Return mDeptID
        End Get
        Set(ByVal value As Integer)
            mDeptID = value
            Session("DeptID") = mDeptID
        End Set
    End Property

    Private mCurrentDR As DataRow
    Public Property CurrentDR() As DataRow
        Get
            mCurrentDR = Session("CurrentDR")
            Return mCurrentDR
        End Get
        Set(ByVal value As DataRow)
            mCurrentDR = value
            Session("CurrentDR") = mCurrentDR
        End Set
    End Property


#End Region


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            '  If (Request.QueryString("IsDataLoaded") = "False") Or (Request.QueryString("IsDataLoaded") = "") Then
            btnAddStaff.Visible = False
            FillDeptDropDown()
            Me.StaffGW = New StaffGateway()
            'End If
        End If
    End Sub

    Private Sub FillDeptDropDown()
        Me.DeptGW = New DeptGateway()
        Me.DeptGW.LoadAll()
        Dept.DataSource = Me.DeptGW.Table
        Dept.DataBind()
        Dept.Items.Insert(0, New ListItem("Select a Department", "Select a Department"))
        Dept.SelectedIndex = 0
    End Sub
    ''' <summary>
    ''' Display gridview using datatable values
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub BindData()
        GridView1.DataSource = Me.StaffGW.Table
        GridView1.DataBind()
    End Sub

    Private Sub RefreshBindData()
        Me.StaffGW.LoadWhere("DeptID = " & Me.DeptID.ToString & " Order By DeptDefault DESC, LastName ASC ")
        BindData()
    End Sub

    Protected Sub ChangeDept(ByVal sender As Object, ByVal e As EventArgs) Handles Dept.SelectedIndexChanged
        Me.DeptID = Dept.SelectedIndex
        Me.StaffGW = Session("StaffGW")
        RefreshBindData()
        btnAddStaff.Visible = True
    End Sub

    ''' <summary>
    ''' Gets triggered when Update link clicked in edit mode
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub GridView1_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles GridView1.RowUpdating
        UpdateRow(e.RowIndex)
    End Sub

    Sub UpdateRow(ByVal currRowIndex As Integer)
        UpdateDataTableWithGridValues(currRowIndex)
        RefreshBindData() 'Bind data to the GridView control.
    End Sub

    ''' <summary>
    ''' Send updated gridview row to data table and commit to db
    ''' </summary>
    ''' <param name="RowIndex"></param>
    ''' <remarks></remarks>
    Private Sub UpdateDataTableWithGridValues(ByVal RowIndex As Integer)
        Dim gridRow As GridViewRow = GridView1.Rows(RowIndex)
        Dim recID As Integer = GridView1.DataKeys(RowIndex).Value.ToString() 'DataKey avoids having to create a boundfield on the id.  Boundfield would have to be readonly=false to be able to access'
        If recID < 0 Then
            InsertRefreshStaffGW(recID)
        End If
        Dim acutalRowIndex = gridRow.DataItemIndex 'dataItemIndex will not equal RowIndex if using paging
        'Update the values.  Have to use DataItemIndex to get row count.  (eg if row 10 is first item on page 2, Rowindex will = 0, and DataItemIndex will = 10
        GridViewToDataTableValMapper(recID, gridRow) ', dg.table.Rows(acutalRowIndex))
        GridView1.EditIndex = -1 'Reset the edit index.  The zero-based index of the row to edit. The default is -1, which indicates that no row is being edited.
    End Sub

    Private Sub InsertRefreshStaffGW(ByRef recID As Integer)
        recID = Me.StaffGW.Insert(Me.CurrentDR)
        Me.DeptID = Session("DeptID")
        RefreshBindData()
    End Sub

    ''' <summary>
    ''' Overwrite datatable row items values with gridview values.  Commit to db
    ''' </summary>
    ''' <param name="RecID"></param>
    ''' <param name="GridRow"></param>
    ''' <remarks></remarks>
    Private Sub GridViewToDataTableValMapper(ByVal RecID As Integer, ByVal GridRow As GridViewRow)
        Dim DTRow As DataRow = Me.StaffGW.RowByKey(RecID, "ID")
        Dim tbLastName As TextBox = GridRow.FindControl("LastName")
        Dim tbFirstName As TextBox = GridRow.FindControl("FirstName")
        Dim tbTel As TextBox = GridRow.FindControl("Tel")
        Dim tbTelExt As TextBox = GridRow.FindControl("TelExt")
        Dim tbFax As TextBox = GridRow.FindControl("Fax")
        Dim tbEmail As TextBox = GridRow.FindControl("Email")
        Dim tbCell As TextBox = GridRow.FindControl("Cell")
        Dim tbNotes As TextBox = GridRow.FindControl("Notes")
        Dim cbActive As CheckBox = GridRow.FindControl("Active")

        With DTRow
            .Item("LastName") = tbLastName.Text
            .Item("FirstName") = tbFirstName.Text
            .Item("Tel") = tbTel.Text
            .Item("TelExt") = tbTelExt.Text
            .Item("Fax") = tbFax.Text
            .Item("Email") = tbEmail.Text
            .Item("Cell") = tbCell.Text
            .Item("Notes") = tbNotes.Text
            .Item("DeptID") = Me.DeptID.ToString
            .Item("LastChanged") = Now()
            .Item("Active") = cbActive.Checked
        End With
        Me.StaffGW.Update(DTRow)
    End Sub

    Protected Sub GridView1_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles GridView1.RowEditing
        EditRow(e.NewEditIndex)
    End Sub

    Sub EditRow(ByVal currRowIndex As Integer)
        GridView1.EditIndex = currRowIndex   'set index number of row being edited.  NewEditIndex evaluates to index of row being edited. 
        SetRecIDCurrent(currRowIndex)   'set current ID as session var
        BindData() 'Bind data to the GridView control.
    End Sub

    ''' <summary>
    ''' Set RecID session var
    ''' </summary>
    ''' <param name="RowIndex"></param>
    ''' <remarks></remarks>
    Private Sub SetRecIDCurrent(ByVal RowIndex As Integer)
        Me.RecIDCurrent = GridView1.DataKeys(RowIndex).Value.ToString()
    End Sub

    Protected Sub GridView1_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles GridView1.RowCancelingEdit
        CancelEditRow()
    End Sub

    Sub CancelEditRow()
        GridView1.EditIndex = -1 'Reset the edit index.
        RefreshBindData() 'Bind data to the GridView control.
    End Sub

    Protected Sub btnAddStaff_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddStaff.Click
        AddStaffProcess()
    End Sub

    Private Sub AddStaffProcess()
        AddStaff()
        EditNewStaff()
    End Sub

    Private Sub AddStaff()
        Dim DTRow As DataRow = Me.StaffGW.Table.NewRow
        With DTRow
            .Item("LastName") = ""
            .Item("FirstName") = ""
            .Item("Tel") = ""
            .Item("TelExt") = ""
            .Item("Fax") = ""
            .Item("Email") = ""
            .Item("Cell") = ""
            .Item("Notes") = ""
            .Item("DeptID") = Me.DeptID.ToString
            .Item("LastChanged") = Now()
            .Item("DeptDefault") = 0
            .Item("LastNameFR") = ""
            .Item("FirstNameFR") = ""
            .Item("Active") = 1
        End With
        Me.StaffGW.Table.Rows.Add(DTRow)
        Me.CurrentDR = DTRow
    End Sub

    Private Sub EditNewStaff()
        Dim currRowIndex As Integer = GridView1.Rows.Count
        GridView1.EditIndex = currRowIndex
        BindData()
    End Sub
End Class