﻿Imports CrystalDecisions.CrystalReports.Engine
Imports System.Web


Partial Public Class AssignStaffMain
    Inherits System.Web.UI.Page


#Region "Class properties"
    Private mAssignmentMgr As StaffAssignmentMgr
    Public Property AssignmentMgr() As StaffAssignmentMgr
        Get
            mAssignmentMgr = Session("AssignmentMgr")
            Return mAssignmentMgr
        End Get
        Set(ByVal value As StaffAssignmentMgr)
            mAssignmentMgr = value
            Session("AssignmentMgr") = mAssignmentMgr
        End Set
    End Property

    Private mStaffGW As StaffGateway
    Public Property StaffGW() As StaffGateway
        Get
            mStaffGW = Session("Staff")
            Return mStaffGW
        End Get
        Set(ByVal value As StaffGateway)
            mStaffGW = value
            Session("Staff") = mStaffGW
        End Set
    End Property

    Private mRecIDCurrent As Integer
    Public Property RecIDCurrent() As Integer
        Get
            mRecIDCurrent = Session("RecIDCurrent")
            Return mRecIDCurrent
        End Get
        Set(ByVal value As Integer)
            mRecIDCurrent = value
            Session("RecIDCurrent") = mRecIDCurrent
        End Set
    End Property

    Private mClient As String
    Public Property Client() As String
        Get
            Return mClient
        End Get
        Set(ByVal value As String)
            mClient = value
        End Set
    End Property

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then

            If (Request.QueryString("IsDataLoaded") = "False") Or (Request.QueryString("IsDataLoaded") = "") Then
                Me.StaffGW = New StaffGateway
                Me.StaffGW.LoadAll()
                Me.AssignmentMgr = New StaffAssignmentMgr
                Me.AssignmentMgr.Lang = "E"
                ClientList.Visible = False
                lblClientName.Visible = False

                'Dim webService As New ClientInfoProd.ClientInfo
                'webService.UseDefaultCredentials = False

                'Session("ClientMgr") = webService.GetAllClient

                If Request.QueryString("Client") <> "" Then
                    Me.AssignmentMgr.LoadByClientID(Request.QueryString("Client"))
                    If Request.QueryString("FromInsertDeptPos") = "Y" Then
                        Me.tbClient.Text = Request.QueryString("Client")
                        SelectedClient()
                    End If
                    BindData()
                Else
                    HideAllButtons()
                End If

                'HideAllButtons()
            End If
        End If

        Me.Client = Me.tbClient.Text
    End Sub

    Sub HideAllButtons()
        btnPreviewRpt.Visible = False
        panelPrint.Visible = False
        rbLang.Visible = False
        btnGo.Visible = False
        panelInsertDept.Visible = False
    End Sub

    ''' <summary>
    ''' Display gridview using datatable values
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub BindData()
        GridView1.DataSource = Me.AssignmentMgr.Table
        GridView1.DataBind()
    End Sub

    ''' <summary>
    ''' Gets triggered when Update link clicked in edit mode
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub GridView1_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles GridView1.RowUpdating
        UpdateRow(e.RowIndex)
    End Sub

    Sub UpdateRow(ByVal currRowIndex As Integer)
        UpdateDataTableWithGridValues(currRowIndex)
        BindData() 'Bind data to the GridView control.
    End Sub

    ''' <summary>
    ''' Send updated gridview row to data table and commit to db
    ''' </summary>
    ''' <param name="RowIndex"></param>
    ''' <remarks></remarks>
    Private Sub UpdateDataTableWithGridValues(ByVal RowIndex As Integer)
        Dim gridRow As GridViewRow = GridView1.Rows(RowIndex)
        Dim recID As Integer = GridView1.DataKeys(RowIndex).Value.ToString() 'DataKey avoids having to create a boundfield on the id.  Boundfield would have to be readonly=false to be able to access'
        Dim acutalRowIndex = gridRow.DataItemIndex 'dataItemIndex will not equal RowIndex if using paging
        'Update the values.  Have to use DataItemIndex to get row count.  (eg if row 10 is first item on page 2, Rowindex will = 0, and DataItemIndex will = 10
        GridViewToDataTableValMapper(recID, gridRow) ', dg.table.Rows(acutalRowIndex))
        GridView1.EditIndex = -1 'Reset the edit index.  The zero-based index of the row to edit. The default is -1, which indicates that no row is being edited.
    End Sub


    ''' <summary>
    ''' Overwrite datatable row items values with gridview values.  Commit to db
    ''' </summary>
    ''' <param name="RecID"></param>
    ''' <param name="GridRow"></param>
    ''' <remarks></remarks>
    Private Sub GridViewToDataTableValMapper(ByVal RecID As Integer, ByVal GridRow As GridViewRow)
        Dim DTRow As DataRow = AssignmentMgr.StaffAssignmentsGW.RowByKey(RecID, "ID")
        Dim ddlStaff As DropDownList = GridRow.FindControl("dropDownStaff")
        Dim staffIDSelected = ddlStaff.SelectedValue
        Dim staffDR As DataRow = Me.StaffGW.RowByKey(staffIDSelected, "ID")
        Dim cbNotDisplayedChecked As Boolean

        If staffDR.Item("DeptDefault") = False Then
            cbNotDisplayedChecked = False
        Else
            cbNotDisplayedChecked = True
        End If


        With DTRow
            .Item("StaffID") = staffIDSelected
            .Item("StaffFirstName") = staffDR.Item("FirstName")
            .Item("StaffLastName") = staffDR.Item("LastName")
            .Item("Tel") = staffDR.Item("Tel")
            .Item("Email") = staffDR.Item("Email")
            .Item("LastChanged") = Now()
            .Item("NotDisplayed") = cbNotDisplayedChecked
        End With
        Me.AssignmentMgr.Update()
    End Sub



    ''' <summary>
    ''' Returns index of datacontrol field in GrdView gridview, where control.headerText = ControlHeaderName
    ''' Use for debugging only.  Not efficient as it iterates through every column in grid until it finds match
    ''' </summary>
    ''' <param name="GrdView"></param>
    ''' <param name="ControlHeaderName"></param>
    ''' <returns>If gridview is onEdit or onUpdating, add 1 to return value to count for the extra edit/update conrtol (which is not considered a data control</returns>
    ''' <remarks>source:http://stackoverflow.com/questions/3925183/method-to-find-gridview-column-index-by-name</remarks>
    Private Function HelperGetIndexOfDataControl(ByVal GrdView As GridView, ByVal ControlHeaderName As String) As Integer
        Dim retval As Integer = -1
        For Each col As DataControlField In GridView1.Columns
            If (col.HeaderText.ToLower.Trim = ControlHeaderName.ToLower.Trim) Then
                retval = GrdView.Columns.IndexOf(col)
            End If
        Next
        Return retval
    End Function

    Protected Sub GridView1_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles GridView1.RowEditing
        EditRow(e.NewEditIndex)
    End Sub

    Sub EditRow(ByVal currRowIndex As Integer)
        GridView1.EditIndex = currRowIndex   'set index number of row being edited.  NewEditIndex evaluates to index of row being edited. 
        SetRecIDCurrent(currRowIndex)   'set current ID as session var
        BindData() 'Bind data to the GridView control.
    End Sub

    Protected Sub GridView1_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles GridView1.RowCancelingEdit
        CancelEditRow()
    End Sub

    Sub CancelEditRow()
        GridView1.EditIndex = -1 'Reset the edit index.
        BindData() 'Bind data to the GridView control.
    End Sub


    ''' <summary>
    ''' Datatable() of staff with deptid = dept of of RecIDCurrent session var
    ''' </summary>
    ''' <returns>DataTable() of staff with deptid = dept of of RecIDCurrent session var</returns>
    ''' <remarks></remarks>
    Public Function GetStaffFullName() As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("ID", Type.GetType("System.Int32"))
        dt.Columns.Add("FullName", Type.GetType("System.String"))
        Dim currDept As Integer = GetDeptIDByRecIDCurrent()
        Dim query = From staff In Me.StaffGW.Table _
                Where (staff.Item("DeptID") = currDept And staff.Item("Active") = True) _
                Order By staff.Item("DeptDefault") Descending, staff.Item("FirstName") Ascending _
                Select dt.Rows.Add(staff.Item("ID"), String.Format("{0} {1}", staff.Item("FirstName"), staff.Item("LastName")))
        query.ToList()   'the .toList just executes the query else it will be delayed and datatable will not be populated
        Return dt
    End Function

    ''' <summary>
    ''' Returns DeptID of datarow where ID = session variable value of RecIDCurrent
    ''' </summary>
    ''' <returns>DeptID of datarow where ID = session variable value of RecIDCurrent</returns>
    ''' <remarks></remarks>
    Private Function GetDeptIDByRecIDCurrent() As Integer
        Return Me.AssignmentMgr.StaffAssignmentsGW.RowValByKey(Me.RecIDCurrent, "ID", "deptID") 'todo:  need wrapper at mgr to return DeptID by row
    End Function

    ''' <summary>
    ''' Set RecID session var
    ''' </summary>
    ''' <param name="RowIndex"></param>
    ''' <remarks></remarks>
    Private Sub SetRecIDCurrent(ByVal RowIndex As Integer)
        Me.RecIDCurrent = GridView1.DataKeys(RowIndex).Value.ToString()
    End Sub

    Protected Sub btnPreviewRpt_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPreviewRpt.Click
        PreviewRpt()
    End Sub

    Sub PreviewRpt()
        Dim url As String = "PreviewSvcContactListRpt.aspx?Lang=" & Me.rbLang.Text.Trim & "&Company=" & Me.rbCompany.Text & "&Client='" & " + escape(""" & (Me.tbClient.Text.Trim) & """)"
        Dim scriptTxt As New StringBuilder()
        scriptTxt.Append("<script type = 'text/javascript'>")
        scriptTxt.Append("window.open('")
        scriptTxt.Append(url)
        scriptTxt.Append(");")
        scriptTxt.Append("</script>")
        ClientScript.RegisterStartupScript(Me.GetType(), "script", scriptTxt.ToString())
    End Sub

    Protected Sub btnFind_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnFind.Click
        FindClient()
    End Sub

    Sub FindClient()
        Dim webService As New ClientInfoProd.ClientInfo
        webService.UseDefaultCredentials = True
        Dim list() As String
        'webService.GetAllClient()
        list = webService.GetListContainingString(tbClient.Text)
        If list.Length > 0 Then
            FillDropDownClientList(list)
        End If
        HideAllButtons()
        ResetGridView()
        lblClientName.Visible = False
    End Sub

    Sub ResetGridView()
        GridView1.DataSource = Nothing
        GridView1.DataBind()
    End Sub

    Sub FillDropDownClientList(ByVal list() As String)
        ClientList.DataSource = list
        ClientList.DataBind()
        ClientList.Visible = True

        Dim longestStr As Integer = FindLongestString(list)
        ClientList.Width = 9 * longestStr
    End Sub

    Function FindLongestString(ByVal List() As String) As Integer
        Dim prevLength As Integer = 0
        For Each Str As String In List
            Dim currLength As Integer = Str.Length
            If currLength > prevLength Then
                prevLength = currLength
            End If
        Next
        Return prevLength
    End Function

    Protected Sub ClientList_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ClientList.SelectedIndexChanged
        SelectedClient()
    End Sub

    Sub SelectedClient()
        Dim strArray As Array = Split(ClientList.SelectedValue, ",")
        If UBound(strArray) > 0 Then
            Me.tbClient.Text = strArray(0).ToString 'ClientList.Text
        End If
        ClientList.Visible = False
        Me.AssignmentMgr = Session("AssignmentMgr")
        If (Me.AssignmentMgr.LoadByClientID(Me.tbClient.Text)) Then
            ViewContactSheetByClient(Me.tbClient.Text)
            panelUpdate.Visible = False
        Else
            btnGo.Visible = True
        End If
    End Sub


    Protected Sub btnGo_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGo.Click
        GoFillClient() 'create new
    End Sub

    Sub GoFillClient()
        Dim client As String = GetClient()
        FillClientLabel(client)
        If Not (Me.AssignmentMgr.LoadByClientID(client)) Then
            Me.AssignmentMgr.CreateInsertLoadStaffAssignmentsForNewClient(client)
        Else
            Me.AssignmentMgr.LoadStaffAssignmentsForExistingClient(client)
        End If
        BindData()
        SetButtons()
    End Sub

    Sub FillClientLabel(ByVal Client As String)
        lblClientName.Visible = True
        lblClientName.Text = "Client: " & Client 'GetClient()
    End Sub

    Private Function GetClient() As String
        Dim clientName As String = tbClient.Text.Substring(tbClient.Text.LastIndexOf(",") + 1)
        Return clientName
    End Function

    Sub ViewContactSheetByClient(ByVal Client As String)
        BindData()
        SetButtons()
    End Sub

    Sub SetButtons()
        btnPreviewRpt.Visible = True
        panelPrint.Visible = True
        rbLang.Visible = True
        rbLang.SelectedIndex = 0
        btnGo.Visible = False
        panelInsertDept.Visible = True
    End Sub

    Protected Sub ColorRowsHiddenRows(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        If (DataBinder.Eval(e.Row.DataItem, "NotDisplayed")) Then
            e.Row.BackColor = Drawing.Color.LightGray
        Else
            e.Row.BackColor = Drawing.Color.Transparent
        End If
    End Sub


    Protected Sub InsertDeptPosition()
        'Dim insertDeptPos As New InsertDeptPosition
        'insertDeptPos.ClientName = Me.Client

        Dim url As String = "InsertDeptPosition.aspx?Client='" & " + escape(""" & (Me.tbClient.Text.Trim) & """)"
        Dim scriptTxt As New StringBuilder()
        scriptTxt.Append("<script type = 'text/javascript'>")
        scriptTxt.Append("window.open('")
        scriptTxt.Append(url)
        scriptTxt.Append(");")
        scriptTxt.Append("</script>")
        ClientScript.RegisterStartupScript(Me.GetType(), "script", scriptTxt.ToString())
    End Sub

End Class