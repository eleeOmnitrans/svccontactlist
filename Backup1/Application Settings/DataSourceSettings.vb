﻿Public Module DataSourceSettings
    Public Const SRCTBLNAMEDOCCAPTURE As String = "dbo_View_TransGroupedRecent"
    Public Const SRCTBLNAMEDOCCAPTURECONFIG As String = "dboView_CaptureAuditConfig"

    Public Const SRCTBLNAMESCANNODE As String = "ScanNodeLogServer"
    Public Const SRCTBLNAMESCANNODECONFIG As String = "ScanNodeLogConfig"

    Public Const SRCTBLNAMEEBILL As String = "dbo_View_FetchPerformanceEbill"
    Public Const SRCTBLNAMEEBILLCONFIG As String = "dboView_FetchEbillConfig"


    'Public Function GetWIPWIPXMLFileNamePrefix()
    '    Return System.Configuration.ConfigurationManager.AppSettings("TestSettingNameInCommonConfig")
    'End Function


    Public Function GetAppDBConnectStr()
        Return String.Format("Data Source={0} ;Initial Catalog={1} ;Integrated Security={2}", GetAppDBDataSource(), GetAppDBInitialCatalog(), GetAppDBIntegratedSecurity())
    End Function


    Public Function GetAppDBConnectStr2()
        Return String.Format("Data Source={0} ;Initial Catalog={1} ;Integrated Security={2}", GetAppDBDataSource2(), GetAppDBInitialCatalog2(), GetAppDBIntegratedSecurity())
    End Function

    Public Function GetAppDBConnectWebStr()
        Return String.Format("Data Source={0} ;Initial Catalog={1} ;User id={2} ;Password={3}", GetAppDBDataSource(), GetAppDBInitialCatalog(), GetAppDBUserID(), GetAppDBPassword())
    End Function

    Public Function GetAppDBDataSource() As String
        Return System.Configuration.ConfigurationManager.AppSettings("DBDataSource")
    End Function

    Public Function GetAppDBDataSource2() As String
        Return System.Configuration.ConfigurationManager.AppSettings("DBDataSource2")
    End Function

    Public Function GetAppDBInitialCatalog() As String
        Return System.Configuration.ConfigurationManager.AppSettings("DBInitialCatalog")
    End Function

    Public Function GetAppDBIntegratedSecurity() As String
        Return System.Configuration.ConfigurationManager.AppSettings("DBIntegratedSecurity")
    End Function

    Public Function GetAppDBInitialCatalog2() As String
        Return System.Configuration.ConfigurationManager.AppSettings("DBInitialCatalog2")
    End Function

    Public Function GetAppDBUserID() As String
        Return System.Configuration.ConfigurationManager.AppSettings("DBUserID")
    End Function

    Public Function GetAppDBPassword() As String
        Return System.Configuration.ConfigurationManager.AppSettings("DBPassword")
    End Function
End Module
