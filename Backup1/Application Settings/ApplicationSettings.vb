﻿Public Module ApplicationSettings
    Public Const TEMPID As Integer = -2         'default QuoteID before object has been sent to database.  Final QuoteID to be assigned by database
    Public Const EXPOLICY As String = "Handled Policy"       'Write error exceptions to Handled policy
    Public Const EXPOLICY_UNHANDLED As String = "Unhandled Policy"       'Write error exceptions to Un-Handled policy
    Public Const LOGPOLICY_GENERAL As String = "OmniGeneral"   'Write to application logs using General Policy
    Public Const LOGPOLICY_DATAACCESS As String = "OmniDataAccess"    'Write to application logs using Database Access layer Policy


    Public Function RunAsTestClient() As Boolean
        Return System.Configuration.ConfigurationManager.AppSettings("AlwaysUseTestAcct")
    End Function


    Public Function GetTestCarrierAccountNumber() As String
        Return System.Configuration.ConfigurationManager.AppSettings("TestCarrierAccountNumber")
    End Function

    Public Function GetInvPostExportPath() As String
        Return System.Configuration.ConfigurationManager.AppSettings("InvPostExportPath")
    End Function

    Public Function GetTestCarrierInvoiceNumber() As String
        Return System.Configuration.ConfigurationManager.AppSettings("TestCarrierInvoiceNumber")
    End Function

    Public Function GetReportExportPath() As String
        Return System.Configuration.ConfigurationManager.AppSettings("ReportExportPath")
    End Function

    Public Function GetClientReportExportPath() As String
        Return System.Configuration.ConfigurationManager.AppSettings("ClientPath")
    End Function

    Public Function GetInternalReportExportPath() As String
        Return System.Configuration.ConfigurationManager.AppSettings("InternalPath")
    End Function




    Public Function GetConfigValue(ByVal KeyVal As String) As String
        Return System.Configuration.ConfigurationManager.AppSettings(KeyVal)
    End Function

End Module
